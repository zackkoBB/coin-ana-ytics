package com.gconsulting.webapp.arbitrage;

import java.lang.Thread.State;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gconsulting.Constants;
import com.gconsulting.model.API;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiId;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.model.Data;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.Orderbook;
import com.gconsulting.webapp.rest.interfaces.ExchangeRateRESTInterface;
import com.gconsulting.webapp.rest.interfaces.HistoricalDataRESTInterface;
import com.gconsulting.webapp.rest.interfaces.RESTInterface;

public class ArbitrageScanner {

	protected final Log log = LogFactory.getLog(getClass());

	private ExchangeManager exchangeManager;
	private Map<String, Orderbook> orderbooksUSD;
	private Map<String, Orderbook> orderbooksEUR;
	private Map<String, Orderbook> orderbooksCNY;
	private Map<String, Orderbook> orderbooksRUB;
	private Map<String, Orderbook> orderbooksGBP;
	private Map<String, List<Data>> historicalDataUSD;
	private Map<String, List<Data>> historicalDataEUR;
	private Map<String, List<Data>> historicalDataCNY;
	private Map<String, List<Data>> historicalDataRUB;
	private Map<String, List<Data>> historicalDataGBP;
	private Map<String, List<ExchangeRate>> exchangeRateUSD;
	private Map<String, List<ExchangeRate>> exchangeRateEUR;
	private Map<String, List<ExchangeRate>> exchangeRateCNY;
	private Map<String, List<ExchangeRate>> exchangeRateRUB;
	private Map<String, List<ExchangeRate>> exchangeRateGBP;
	private Thread arbitrageThread;
	private Client client;

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final ArbitrageScanner INSTANCE = new ArbitrageScanner();

	private ArbitrageScanner() {

		ClientConfig config = new ClientConfig();
		client = ClientBuilder.newClient(config);
		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	public static ArbitrageScanner getInstance() {
		return INSTANCE;
	}

	public static String httpGet(String urlStr) throws IOException {
		
		URL url = new URL(urlStr);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		if (conn.getResponseCode() != 200) {
			throw new IOException(conn.getResponseMessage());
		}

		// Buffer the result into a string
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();

		conn.disconnect();
		return sb.toString();
	}

	/**
	 * Read a GET JSON API
	 * 
	 * @param source
	 *            API for that REST API
	 * @return JSONObject JSON object read
	 */
	private JSONObject getAPI(API source) {

		// log.info("WebTarget target = client.target(getBaseURI(source.getAddress()));");
		WebTarget target = client.target(getBaseURI(source.getAddress()));
		Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
		// log.info("Response response = builder.get();");
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		// log.info("return new JSONObject(response.readEntity(String.class));");
		return new JSONObject(response.readEntity(String.class));
	}

	/**
	 * Read a GET Text API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return JSONObject JSON object read
	 */
	private String getAPIText(API source) {

		// log.info("WebTarget target = client.target(getBaseURI(source.getAddress()));");
		WebTarget target = client.target(getBaseURI(source.getAddress()));
		Builder builder = target.request(MediaType.APPLICATION_XML);
		// log.info("Response response = builder.get();");
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		// log.info("return response.readEntity(String.class);");
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

	public boolean arbitrageThreadIsAlive() {
		return arbitrageThread.isAlive();
	}

	public boolean arbitrageThreadIsInterrupted() {
		return arbitrageThread.isInterrupted();
	}

	public void arbitrageThreadCheckAccess() {
		arbitrageThread.checkAccess();
	}

	public State arbitrageThreadState() {
		return arbitrageThread.getState();
	}

	public void arbitrageThreadRestart() {

		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	/**
	 * Get Orderbook in USD
	 * 
	 * @return Map<String, Orderbook> USD orderbook
	 */
	public Map<String, Orderbook> getOrderbooksUSD() {
		return orderbooksUSD;
	}

	/**
	 * Set Orderbook in USD
	 * 
	 * @param Map
	 *            <String, Orderbook> USD orderbook
	 */
	public void setOrderbooksUSD(Map<String, Orderbook> orderbooksUSD) {
		this.orderbooksUSD = orderbooksUSD;
	}

	/**
	 * Get Orderbook in EUR
	 * 
	 * @return Map<String, Orderbook> EUR orderbook
	 */
	public Map<String, Orderbook> getOrderbooksEUR() {
		return orderbooksEUR;
	}

	/**
	 * Set Orderbook in EUR
	 * 
	 * @param Map
	 *            <String, Orderbook> EUR orderbook
	 */
	public void setOrderbooksEUR(Map<String, Orderbook> orderbooksEUR) {
		this.orderbooksEUR = orderbooksEUR;
	}

	/**
	 * Get Orderbook in CNY
	 * 
	 * @return Map<String, Orderbook> CNY orderbook
	 */
	public Map<String, Orderbook> getOrderbooksCNY() {
		return orderbooksCNY;
	}

	/**
	 * Set Orderbook in CNY
	 * 
	 * @param Map
	 *            <String, Orderbook> CNY orderbook
	 */
	public void setOrderbooksCNY(Map<String, Orderbook> orderbooksCNY) {
		this.orderbooksCNY = orderbooksCNY;
	}

	/**
	 * Get Orderbook in RUB
	 * 
	 * @return Map<String, Orderbook> RUB orderbook
	 */
	public Map<String, Orderbook> getOrderbooksRUB() {
		return orderbooksRUB;
	}

	/**
	 * Set Orderbook in RUB
	 * 
	 * @param Map
	 *            <String, Orderbook> RUB orderbook
	 */
	public void setOrderbooksRUB(Map<String, Orderbook> orderbooksRUB) {
		this.orderbooksRUB = orderbooksRUB;
	}

	/**
	 * Get Orderbook in GBP
	 * 
	 * @return Map<String, Orderbook> GBP orderbook
	 */
	public Map<String, Orderbook> getOrderbooksGBP() {
		return orderbooksGBP;
	}

	/**
	 * Set Orderbook in GBP
	 * 
	 * @param Map
	 *            <String, Orderbook> GBP orderbook
	 */
	public void setOrderbooksGBP(Map<String, Orderbook> orderbooksGBP) {
		this.orderbooksGBP = orderbooksGBP;
	}

	/**
	 * Get HistoricalDataUSD
	 * 
	 * @return Map<String, List<Data>> HistoricalData
	 */
	public Map<String, List<Data>> getHistoricalDataUSD() {
		return historicalDataUSD;
	}

	/**
	 * Set HistoricalDataUSD
	 * 
	 * @param Map
	 *            <String, List<Data>> HistoricalDataUSD
	 */
	public void setHistoricalDataUSD(Map<String, List<Data>> historicalDataUSD) {
		this.historicalDataUSD = historicalDataUSD;
	}

	public Map<String, List<Data>> getHistoricalDataEUR() {
		return historicalDataEUR;
	}

	public void setHistoricalDataEUR(Map<String, List<Data>> historicalDataEUR) {
		this.historicalDataEUR = historicalDataEUR;
	}

	public Map<String, List<Data>> getHistoricalDataCNY() {
		return historicalDataCNY;
	}

	public void setHistoricalDataCNY(Map<String, List<Data>> historicalDataCNY) {
		this.historicalDataCNY = historicalDataCNY;
	}

	public Map<String, List<Data>> getHistoricalDataRUB() {
		return historicalDataRUB;
	}

	public void setHistoricalDataRUB(Map<String, List<Data>> historicalDataRUB) {
		this.historicalDataRUB = historicalDataRUB;
	}

	public Map<String, List<Data>> getHistoricalDataGBP() {
		return historicalDataGBP;
	}

	public void setHistoricalDataGBP(Map<String, List<Data>> historicalDataGBP) {
		this.historicalDataGBP = historicalDataGBP;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateUSD() {
		return exchangeRateUSD;
	}

	public void setExchangeRateUSD(
			Map<String, List<ExchangeRate>> exchangeRateUSD) {
		this.exchangeRateUSD = exchangeRateUSD;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateEUR() {
		return exchangeRateEUR;
	}

	public void setExchangeRateEUR(
			Map<String, List<ExchangeRate>> exchangeRateEUR) {
		this.exchangeRateEUR = exchangeRateEUR;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateCNY() {
		return exchangeRateCNY;
	}

	public void setExchangeRateCNY(
			Map<String, List<ExchangeRate>> exchangeRateCNY) {
		this.exchangeRateCNY = exchangeRateCNY;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateRUB() {
		return exchangeRateRUB;
	}

	public void setExchangeRateRUB(
			Map<String, List<ExchangeRate>> exchangeRateRUB) {
		this.exchangeRateRUB = exchangeRateRUB;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateGBP() {
		return exchangeRateGBP;
	}

	public void setExchangeRateGBP(
			Map<String, List<ExchangeRate>> exchangeRateGBP) {
		this.exchangeRateGBP = exchangeRateGBP;
	}

	/**
	 * Set ExchangeManager
	 * 
	 * @param exchangeManager
	 */
	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	/**
	 * ArbitrageScannerThread for retrieving arbs opportunities across different
	 * markets
	 * 
	 * @author zackko
	 *
	 */
	private class ArbitrageScannerThread implements Runnable {

		private boolean stop = false;
		private List<API> sources;
		private List<API> historicalDataSources;
		private Exchange historicalDataExchange;
		private List<API> exchangeRateSources;
		private Exchange exchangeRateExchange;

		/**
		 * Init usdMarket and sources for retrieving arbs
		 * 
		 */
		private void init() {

			historicalDataExchange = exchangeManager
					.getExchangeByCode(Constants.HISTORICAL_DATA_EXCHANGE);
			historicalDataSources = exchangeManager
					.getApiByExchange(historicalDataExchange);
			exchangeRateExchange = exchangeManager
					.getExchangeByCode(Constants.EXCHANGE_RATE_EXCHANGE);
			exchangeRateSources = exchangeManager
					.getApiByExchange(exchangeRateExchange);
			List<Exchange> exchanges = exchangeManager.getAllExchange();
			Market usdMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_USD);
			Market eurMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_EUR);
			Market cnyMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_CNY);
			Market rubMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_RUB);
			Market gbpMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_GBP);
			sources = new ArrayList<>();
			API temp = null;
			for (Exchange exchange : exchanges) {
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, usdMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, eurMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, cnyMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, rubMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, gbpMarket)));
				if (temp != null) {
					sources.add(temp);
				}
			}
		}

		/**
		 * Get historical data marketPrefix from web
		 * 
		 * @return Map<String, List<Data>> historical data marketPrefix
		 *         retrieved
		 */
		private Map<String, List<Data>> getHistoricalData(String marketPrefix) {

			Map<String, List<Data>> historicalDataLocal = new TreeMap<String, List<Data>>();
			int i = 0;
			for (API source : historicalDataSources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							i++;
							log.info(i + ") " + marketPrefix + " "
									+ source.getExchange().getCode() + " "
									+ source.getAddress());
							String response = getAPIText(source);
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE
											+ "."
											+ Constants.HISTORICAL_DATA_EXCHANGE
											+ "Interface");
							HistoricalDataRESTInterface restInterface = (HistoricalDataRESTInterface) restInterfaceClass
									.newInstance();
							/*
							 * Save to local structure
							 */
							historicalDataLocal.put(source.getFeeType()
									.getMarket().getCode(),
									restInterface.getHistoricalData(response));
						} catch (Exception e) {
							log.error(" Cannot get the HistoricalData: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							e.printStackTrace();
						}
					}
				}
			}
			return historicalDataLocal;
		}

		/**
		 * Get exchange rate marketPrefix from web
		 * 
		 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
		 *         retrieved
		 */
		private Map<String, List<ExchangeRate>> getExchangeRate(
				String marketPrefix) {

			Map<String, List<ExchangeRate>> exchangeRateLocal = new TreeMap<String, List<ExchangeRate>>();
			int i = 0;
			for (API source : exchangeRateSources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							i++;
							log.info(i + ") " + marketPrefix + " "
									+ source.getExchange().getCode() + " "
									+ source.getAddress());
							JSONObject response = getAPI(source);
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE + "."
											+ Constants.EXCHANGE_RATE_EXCHANGE
											+ "Interface");
							ExchangeRateRESTInterface restInterface = (ExchangeRateRESTInterface) restInterfaceClass
									.newInstance();
							/*
							 * Save to local structure
							 */
							exchangeRateLocal.put(source.getFeeType()
									.getMarket().getCode(),
									restInterface.getExchangeRate(response));
						} catch (Exception e) {
							log.error(" Cannot get the ExchangeRate: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							e.printStackTrace();
						}
					}
				}
			}
			return exchangeRateLocal;
		}

		/**
		 * Get Orderbooks data from web
		 * 
		 * @return Map<String, Orderbook> orderbooks retrieved
		 */
		private Map<String, Orderbook> getOrderbooks(String marketPrefix) {

			Map<String, Orderbook> orderbooks = new HashMap<String, Orderbook>();
			int i = 0;
			for (API source : sources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							i++;
							log.info(i + ") " + marketPrefix + " "
									+ source.getExchange().getCode() + " "
									+ source.getAddress());
							JSONObject response = getAPI(source);
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE + "."
											+ source.getExchange().getCode()
											+ "Interface");
							RESTInterface restInterface = (RESTInterface) restInterfaceClass
									.newInstance();
							orderbooks.put(source.getExchange().getCode(),
									restInterface.getOrderbook(response,
											marketPrefix));
						} catch (Exception e) {
							log.error(" Cannot get the Orderbook: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							e.printStackTrace();
						}
					}
				}
			}
			return orderbooks;
		}

		/**
		 * Run method of this thread
		 */
		public void run() {

			int i = 0;
			init();
			while (!stop) {
				try {
					if ((i % Constants.REST_HISTORICALA_DATA_POLLING_INTERVAL) == 0) {
						historicalDataUSD = getHistoricalData(Constants.MARKET_USD);
						historicalDataEUR = getHistoricalData(Constants.MARKET_EUR);
						historicalDataCNY = getHistoricalData(Constants.MARKET_CNY);
						historicalDataRUB = getHistoricalData(Constants.MARKET_RUB);
						historicalDataGBP = getHistoricalData(Constants.MARKET_GBP);
						exchangeRateUSD = getExchangeRate(Constants.MARKET_USD);
						exchangeRateEUR = getExchangeRate(Constants.MARKET_EUR);
						exchangeRateCNY = getExchangeRate(Constants.MARKET_CNY);
						exchangeRateRUB = getExchangeRate(Constants.MARKET_RUB);
						exchangeRateGBP = getExchangeRate(Constants.MARKET_GBP);
						i = 0;
					}
					orderbooksUSD = getOrderbooks(Constants.MARKET_BTC_USD);
					orderbooksEUR = getOrderbooks(Constants.MARKET_BTC_EUR);
					orderbooksCNY = getOrderbooks(Constants.MARKET_BTC_CNY);
					orderbooksRUB = getOrderbooks(Constants.MARKET_BTC_RUB);
					orderbooksGBP = getOrderbooks(Constants.MARKET_BTC_GBP);
					System.gc();
					i++;
					Thread.sleep(Constants.REST_ARBITRAGE_POLLING_INTERVAL);
				} catch (Throwable e) {
					log.error("Throwable: " + e.getMessage());
					e.printStackTrace();
				}
			}
			client.close();
		}
	}
}
