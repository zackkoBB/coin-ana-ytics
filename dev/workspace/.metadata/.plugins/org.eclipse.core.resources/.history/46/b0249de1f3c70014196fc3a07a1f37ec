package com.gconsulting.webapp.arbitrage;

import java.lang.Thread.State;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gconsulting.Constants;
import com.gconsulting.model.API;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiId;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.model.Data;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.Orderbook;
import com.gconsulting.webapp.rest.interfaces.ExchangeRateRESTInterface;
import com.gconsulting.webapp.rest.interfaces.HistoricalDataRESTInterface;
import com.gconsulting.webapp.rest.interfaces.RESTInterface;
import com.gconsulting.webapp.util.RESTUtils;

public class ArbitrageScanner {

	protected final Log log = LogFactory.getLog(getClass());

	private ExchangeManager exchangeManager;
	private Map<String, Orderbook> orderbooksUSD;
	private Map<String, Orderbook> orderbooksEUR;
	private Map<String, Orderbook> orderbooksCNY;
	private Map<String, Orderbook> orderbooksRUB;
	private Map<String, Orderbook> orderbooksGBP;
	private Map<String, Orderbook> orderbooksUSDDelayed;
	private Map<String, Orderbook> orderbooksEURDelayed;
	private Map<String, Orderbook> orderbooksCNYDelayed;
	private Map<String, Orderbook> orderbooksRUBDelayed;
	private Map<String, Orderbook> orderbooksGBPDelayed;
	private Map<String, List<Data>> historicalDataUSD;
	private Map<String, List<Data>> historicalDataEUR;
	private Map<String, List<Data>> historicalDataCNY;
	private Map<String, List<Data>> historicalDataRUB;
	private Map<String, List<Data>> historicalDataGBP;
	private Map<String, List<ExchangeRate>> exchangeRateUSD;
	private Map<String, List<ExchangeRate>> exchangeRateEUR;
	private Map<String, List<ExchangeRate>> exchangeRateCNY;
	private Map<String, List<ExchangeRate>> exchangeRateRUB;
	private Map<String, List<ExchangeRate>> exchangeRateGBP;
	private Map<String, ExchangeRate> crossCurrencyExchangeRate;
	private Thread arbitrageThread;

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final ArbitrageScanner INSTANCE = new ArbitrageScanner();

	private ArbitrageScanner() {

		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	public static ArbitrageScanner getInstance() {
		return INSTANCE;
	}

	public boolean arbitrageThreadIsAlive() {
		return arbitrageThread.isAlive();
	}

	public boolean arbitrageThreadIsInterrupted() {
		return arbitrageThread.isInterrupted();
	}

	public void arbitrageThreadCheckAccess() {
		arbitrageThread.checkAccess();
	}

	public State arbitrageThreadState() {
		return arbitrageThread.getState();
	}

	public void arbitrageThreadRestart() {

		ArbitrageScannerThread arbitrageRunnable = new ArbitrageScannerThread();
		arbitrageThread = new Thread(arbitrageRunnable);
		arbitrageThread
				.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
					@Override
					public void uncaughtException(Thread t, Throwable e) {
						log.error("ERROR! An exception occurred in "
								+ t.getName() + ". Cause: " + e.getMessage());
					}
				});
		arbitrageThread.start();
	}

	/**
	 * Get Orderbook in USD
	 * 
	 * @return Map<String, Orderbook> USD orderbook
	 */
	public Map<String, Orderbook> getOrderbooksUSD() {
		return orderbooksUSD;
	}

	/**
	 * Set Orderbook in USD
	 * 
	 * @param Map
	 *            <String, Orderbook> USD orderbook
	 */
	public void setOrderbooksUSD(Map<String, Orderbook> orderbooksUSD) {
		this.orderbooksUSD = orderbooksUSD;
	}

	/**
	 * Get Orderbook in EUR
	 * 
	 * @return Map<String, Orderbook> EUR orderbook
	 */
	public Map<String, Orderbook> getOrderbooksEUR() {
		return orderbooksEUR;
	}

	/**
	 * Set Orderbook in EUR
	 * 
	 * @param Map
	 *            <String, Orderbook> EUR orderbook
	 */
	public void setOrderbooksEUR(Map<String, Orderbook> orderbooksEUR) {
		this.orderbooksEUR = orderbooksEUR;
	}

	/**
	 * Get Orderbook in CNY
	 * 
	 * @return Map<String, Orderbook> CNY orderbook
	 */
	public Map<String, Orderbook> getOrderbooksCNY() {
		return orderbooksCNY;
	}

	/**
	 * Set Orderbook in CNY
	 * 
	 * @param Map
	 *            <String, Orderbook> CNY orderbook
	 */
	public void setOrderbooksCNY(Map<String, Orderbook> orderbooksCNY) {
		this.orderbooksCNY = orderbooksCNY;
	}

	/**
	 * Get Orderbook in RUB
	 * 
	 * @return Map<String, Orderbook> RUB orderbook
	 */
	public Map<String, Orderbook> getOrderbooksRUB() {
		return orderbooksRUB;
	}

	/**
	 * Set Orderbook in RUB
	 * 
	 * @param Map
	 *            <String, Orderbook> RUB orderbook
	 */
	public void setOrderbooksRUB(Map<String, Orderbook> orderbooksRUB) {
		this.orderbooksRUB = orderbooksRUB;
	}

	/**
	 * Get Orderbook in GBP
	 * 
	 * @return Map<String, Orderbook> GBP orderbook
	 */
	public Map<String, Orderbook> getOrderbooksGBP() {
		return orderbooksGBP;
	}

	/**
	 * Set Orderbook in GBP
	 * 
	 * @param Map
	 *            <String, Orderbook> GBP orderbook
	 */
	public void setOrderbooksGBP(Map<String, Orderbook> orderbooksGBP) {
		this.orderbooksGBP = orderbooksGBP;
	}

	/**
	 * Get HistoricalDataUSD
	 * 
	 * @return Map<String, List<Data>> HistoricalData
	 */
	public Map<String, List<Data>> getHistoricalDataUSD() {
		return historicalDataUSD;
	}

	/**
	 * Set HistoricalDataUSD
	 * 
	 * @param Map
	 *            <String, List<Data>> HistoricalDataUSD
	 */
	public void setHistoricalDataUSD(Map<String, List<Data>> historicalDataUSD) {
		this.historicalDataUSD = historicalDataUSD;
	}

	public Map<String, List<Data>> getHistoricalDataEUR() {
		return historicalDataEUR;
	}

	public void setHistoricalDataEUR(Map<String, List<Data>> historicalDataEUR) {
		this.historicalDataEUR = historicalDataEUR;
	}

	public Map<String, List<Data>> getHistoricalDataCNY() {
		return historicalDataCNY;
	}

	public void setHistoricalDataCNY(Map<String, List<Data>> historicalDataCNY) {
		this.historicalDataCNY = historicalDataCNY;
	}

	public Map<String, List<Data>> getHistoricalDataRUB() {
		return historicalDataRUB;
	}

	public void setHistoricalDataRUB(Map<String, List<Data>> historicalDataRUB) {
		this.historicalDataRUB = historicalDataRUB;
	}

	public Map<String, List<Data>> getHistoricalDataGBP() {
		return historicalDataGBP;
	}

	public void setHistoricalDataGBP(Map<String, List<Data>> historicalDataGBP) {
		this.historicalDataGBP = historicalDataGBP;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateUSD() {
		return exchangeRateUSD;
	}

	public void setExchangeRateUSD(
			Map<String, List<ExchangeRate>> exchangeRateUSD) {
		this.exchangeRateUSD = exchangeRateUSD;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateEUR() {
		return exchangeRateEUR;
	}

	public void setExchangeRateEUR(
			Map<String, List<ExchangeRate>> exchangeRateEUR) {
		this.exchangeRateEUR = exchangeRateEUR;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateCNY() {
		return exchangeRateCNY;
	}

	public void setExchangeRateCNY(
			Map<String, List<ExchangeRate>> exchangeRateCNY) {
		this.exchangeRateCNY = exchangeRateCNY;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateRUB() {
		return exchangeRateRUB;
	}

	public void setExchangeRateRUB(
			Map<String, List<ExchangeRate>> exchangeRateRUB) {
		this.exchangeRateRUB = exchangeRateRUB;
	}

	public Map<String, List<ExchangeRate>> getExchangeRateGBP() {
		return exchangeRateGBP;
	}

	public void setExchangeRateGBP(
			Map<String, List<ExchangeRate>> exchangeRateGBP) {
		this.exchangeRateGBP = exchangeRateGBP;
	}

	public Map<String, ExchangeRate> getCrossCurrencyExchangeRate() {
		return crossCurrencyExchangeRate;
	}

	public void setCrossCurrencyExchangeRate(
			Map<String, ExchangeRate> crossCurrencyExchangeRate) {
		this.crossCurrencyExchangeRate = crossCurrencyExchangeRate;
	}

	public Map<String, Orderbook> getOrderbooksUSDDelayed() {
		return orderbooksUSDDelayed;
	}

	public void setOrderbooksUSDDelayed(
			Map<String, Orderbook> orderbooksUSDDelayed) {
		this.orderbooksUSDDelayed = orderbooksUSDDelayed;
	}

	public Map<String, Orderbook> getOrderbooksEURDelayed() {
		return orderbooksEURDelayed;
	}

	public void setOrderbooksEURDelayed(
			Map<String, Orderbook> orderbooksEURDelayed) {
		this.orderbooksEURDelayed = orderbooksEURDelayed;
	}

	public Map<String, Orderbook> getOrderbooksCNYDelayed() {
		return orderbooksCNYDelayed;
	}

	public void setOrderbooksCNYDelayed(
			Map<String, Orderbook> orderbooksCNYDelayed) {
		this.orderbooksCNYDelayed = orderbooksCNYDelayed;
	}

	public Map<String, Orderbook> getOrderbooksRUBDelayed() {
		return orderbooksRUBDelayed;
	}

	public void setOrderbooksRUBDelayed(
			Map<String, Orderbook> orderbooksRUBDelayed) {
		this.orderbooksRUBDelayed = orderbooksRUBDelayed;
	}

	public Map<String, Orderbook> getOrderbooksGBPDelayed() {
		return orderbooksGBPDelayed;
	}

	public void setOrderbooksGBPDelayed(
			Map<String, Orderbook> orderbooksGBPDelayed) {
		this.orderbooksGBPDelayed = orderbooksGBPDelayed;
	}

	/**
	 * Set ExchangeManager
	 * 
	 * @param exchangeManager
	 */
	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	/**
	 * ArbitrageScannerThread for retrieving arbs opportunities across different
	 * markets
	 * 
	 * @author zackko
	 *
	 */
	private class ArbitrageScannerThread implements Runnable {

		private boolean stop = false;
		private List<API> sources;
		private List<API> historicalDataSources;
		private Exchange historicalDataExchange;
		private List<API> exchangeRateSources;
		private Exchange exchangeRateExchange;
		private Exchange crossCurrencyExchangeRatesExchange;
		private List<API> crossCurrencyExchangeRatesSources;

		/**
		 * Init usdMarket and sources for retrieving arbs
		 * 
		 */
		private void init() {

			historicalDataExchange = exchangeManager
					.getExchangeByCode(Constants.HISTORICAL_DATA_EXCHANGE);
			historicalDataSources = exchangeManager
					.getApiByExchange(historicalDataExchange);
			exchangeRateExchange = exchangeManager
					.getExchangeByCode(Constants.EXCHANGE_RATE_EXCHANGE);
			exchangeRateSources = exchangeManager
					.getApiByExchange(exchangeRateExchange);
			crossCurrencyExchangeRatesExchange = exchangeManager
					.getExchangeByCode(Constants.CROSS_CURRENCY_EXCHANGE_RATE_EXCHANGE);
			crossCurrencyExchangeRatesSources = exchangeManager
					.getApiByExchange(crossCurrencyExchangeRatesExchange);
			List<Exchange> exchanges = exchangeManager.getAllExchange();
			Market usdMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_USD);
			Market eurMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_EUR);
			Market cnyMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_CNY);
			Market rubMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_RUB);
			Market gbpMarket = exchangeManager
					.getMarketByCode(Constants.MARKET_BTC_GBP);
			sources = new ArrayList<>();
			API temp = null;
			for (Exchange exchange : exchanges) {
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, usdMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, eurMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, cnyMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, rubMarket)));
				if (temp != null) {
					sources.add(temp);
				}
				temp = exchangeManager
						.getApiById(new FeeApiId(exchange, new FeeApiType(
								Constants.API_TYPE_ORDERBOOK, gbpMarket)));
				if (temp != null) {
					sources.add(temp);
				}
			}
		}

		/**
		 * Get historical data marketPrefix from web
		 * 
		 * @return Map<String, List<Data>> historical data marketPrefix
		 *         retrieved
		 */
		private Map<String, List<Data>> getHistoricalData(String marketPrefix) {

			Map<String, List<Data>> historicalDataLocal = new TreeMap<String, List<Data>>();
			// int i = 0;
			for (API source : historicalDataSources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							// i++;
							// log.info(i + ") " + marketPrefix + " "
							// + source.getExchange().getCode() + " "
							// + source.getAddress());
							String response = RESTUtils.getInstance().getAPI(
									source);
							// String response = getAPIHTTPGet(source);
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE
											+ "."
											+ Constants.HISTORICAL_DATA_EXCHANGE
											+ "Interface");
							HistoricalDataRESTInterface restInterface = (HistoricalDataRESTInterface) restInterfaceClass
									.newInstance();
							/*
							 * Save to local structure
							 */
							historicalDataLocal.put(source.getFeeType()
									.getMarket().getCode(),
									restInterface.getHistoricalData(response));
						} catch (Exception e) {
							log.error(" Cannot get the HistoricalData: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							// e.printStackTrace();
						}
					}
				}
			}
			return historicalDataLocal;
		}

		/**
		 * Get exchange rate marketPrefix from web
		 * 
		 * @return Map<String, List<ExchangeRate>> exchange rate marketPrefix
		 *         retrieved
		 */
		private Map<String, List<ExchangeRate>> getExchangeRate(
				String marketPrefix) {

			Map<String, List<ExchangeRate>> exchangeRateLocal = new TreeMap<String, List<ExchangeRate>>();
			// int i = 0;
			for (API source : exchangeRateSources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							// i++;
							// log.info(i + ") " + marketPrefix + " "
							// + source.getExchange().getCode() + " "
							// + source.getAddress());
							JSONObject response = new JSONObject(RESTUtils
									.getInstance().getAPI(source));
							// JSONObject response = new
							// JSONObject(getAPIHTTPGet(source));
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE + "."
											+ Constants.EXCHANGE_RATE_EXCHANGE
											+ "Interface");
							ExchangeRateRESTInterface restInterface = (ExchangeRateRESTInterface) restInterfaceClass
									.newInstance();
							/*
							 * Save to local structure
							 */
							exchangeRateLocal.put(source.getFeeType()
									.getMarket().getCode(),
									restInterface.getExchangeRate(response));
						} catch (Exception e) {
							log.error(" Cannot get the ExchangeRate: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							// e.printStackTrace();
						}
					}
				}
			}
			return exchangeRateLocal;
		}

		/**
		 * Get cross currency exchange rates from web
		 * 
		 * @return Map<String, ExchangeRate> cross currency exchange Rates
		 */
		private Map<String, ExchangeRate> getCrossCurrencyExchangeRates() {

			Map<String, ExchangeRate> exchangeRateLocal = new TreeMap<String, ExchangeRate>();
			// int i = 0;
			for (API source : crossCurrencyExchangeRatesSources) {
				if (source.getExchange().getRestInterface()) {
					try {
						// i++;
						// log.info(i + ") " + " "
						// + source.getExchange().getCode() + " "
						// + source.getAddress());
						JSONObject response = new JSONObject(RESTUtils
								.getInstance().getAPI(source));
						Class<?> restInterfaceClass = Class
								.forName(Constants.REST_BASE_PACKAGE
										+ "."
										+ Constants.CROSS_CURRENCY_EXCHANGE_RATE_EXCHANGE
										+ "Interface");
						ExchangeRateRESTInterface restInterface = (ExchangeRateRESTInterface) restInterfaceClass
								.newInstance();
						/*
						 * Save to local structure
						 */
						exchangeRateLocal.put(source.getFeeType().getMarket()
								.getCode(),
								restInterface.getExchangeRate(response).get(0));
					} catch (Exception e) {
						log.error(" Cannot get the CrossCurrencyExchangeRate: "
								+ source.getExchange().getName() + " "
								+ e.getMessage());
						// e.printStackTrace();
					}
				}
			}
			return exchangeRateLocal;
		}

		/**
		 * Get Orderbooks data from web
		 * 
		 * @return Map<String, Orderbook> orderbooks retrieved
		 */
		private Map<String, Orderbook> getOrderbooks(String marketPrefix) {

			Map<String, Orderbook> orderbooks = new HashMap<String, Orderbook>();
			 int i = 0;
			for (API source : sources) {
				if (source.getExchange().getRestInterface()) {
					if (source.getFeeType().getMarket().getCode()
							.startsWith(marketPrefix)) {
						try {
							 i++;
							 log.info(i + ") " + marketPrefix + " "
							 + source.getExchange().getCode() + " "
							 + source.getAddress());
							JSONObject response = new JSONObject(RESTUtils
									.getInstance().getAPI(source));
							// JSONObject response = new
							// JSONObject(getAPIHTTPGet(source));
							Class<?> restInterfaceClass = Class
									.forName(Constants.REST_BASE_PACKAGE + "."
											+ source.getExchange().getCode()
											+ "Interface");
							RESTInterface restInterface = (RESTInterface) restInterfaceClass
									.newInstance();
							orderbooks.put(source.getExchange().getCode(),
									restInterface.getOrderbook(response,
											marketPrefix));
						} catch (Exception e) {
							log.error(" Cannot get the Orderbook: "
									+ source.getExchange().getName() + " "
									+ e.getMessage());
							// e.printStackTrace();
						}
					}
				}
			}
			return orderbooks;
		}

		/**
		 * Run method of this thread
		 */
		public void run() {

			int historicalDataPollingInterval = 0;
			int delayedDataPollingInterval = 0;
			init();
			while (!stop) {
				try {
					if ((historicalDataPollingInterval % Constants.REST_HISTORICALA_DATA_POLLING_INTERVAL) == 0) {
						historicalDataUSD = getHistoricalData(Constants.MARKET_USD);
						historicalDataEUR = getHistoricalData(Constants.MARKET_EUR);
						historicalDataCNY = getHistoricalData(Constants.MARKET_CNY);
						historicalDataRUB = getHistoricalData(Constants.MARKET_RUB);
						historicalDataGBP = getHistoricalData(Constants.MARKET_GBP);
						exchangeRateUSD = getExchangeRate(Constants.MARKET_USD);
						exchangeRateEUR = getExchangeRate(Constants.MARKET_EUR);
						exchangeRateCNY = getExchangeRate(Constants.MARKET_CNY);
						exchangeRateRUB = getExchangeRate(Constants.MARKET_RUB);
						exchangeRateGBP = getExchangeRate(Constants.MARKET_GBP);
						historicalDataPollingInterval = 0;
					}
					orderbooksUSD = getOrderbooks(Constants.MARKET_BTC_USD);
					orderbooksEUR = getOrderbooks(Constants.MARKET_BTC_EUR);
					orderbooksCNY = getOrderbooks(Constants.MARKET_BTC_CNY);
					orderbooksRUB = getOrderbooks(Constants.MARKET_BTC_RUB);
					orderbooksGBP = getOrderbooks(Constants.MARKET_BTC_GBP);
					if ((delayedDataPollingInterval % Constants.REST_DELAYED_DATA_POLLING_INTERVAL) == 0) {
						orderbooksUSDDelayed = orderbooksUSD;
						orderbooksEURDelayed = orderbooksEUR;
						orderbooksCNYDelayed = orderbooksCNY;
						orderbooksRUBDelayed = orderbooksRUB;
						orderbooksGBPDelayed = orderbooksGBP;
						delayedDataPollingInterval = 0;
					}
					crossCurrencyExchangeRate = getCrossCurrencyExchangeRates();
					System.gc();
					historicalDataPollingInterval++;
					delayedDataPollingInterval++;
					Thread.sleep(Constants.REST_ARBITRAGE_POLLING_INTERVAL);
				} catch (Throwable e) {
					log.error("Throwable: " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
}
