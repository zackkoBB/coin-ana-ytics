package com.gconsulting.rest.webapp.model;

public class Order {

	private Double price;
	private Double quantity;

	public Order() {
		super();
	}

	public Order(Double price, Double quantity) {
		super();
		this.price = price;
		this.quantity = quantity;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
}
