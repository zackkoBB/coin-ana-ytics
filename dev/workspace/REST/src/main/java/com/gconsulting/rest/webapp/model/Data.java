package com.gconsulting.rest.webapp.model;

public class Data {

	private Long time;
	private Double price;
	private Double quantity;

	public Data() {
		super();
	}

	public Data(Long time, Double price, Double quantity) {
		super();
		this.time = time;
		this.price = price;
		this.quantity = quantity;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

}
