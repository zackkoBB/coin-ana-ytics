package com.gconsulting.rest.interfaces;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Orderbook;

public interface RESTInterface {

	public List<Market> getPairs(JSONObject source);
	public Orderbook getOrderbook(JSONObject source) throws JSONException;
}
