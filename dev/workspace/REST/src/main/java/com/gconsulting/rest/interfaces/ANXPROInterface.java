package com.gconsulting.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class ANXPROInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
		JSONObject data = source.getJSONObject("data");
		JSONArray asks = data.getJSONArray("asks");
		JSONArray bids = data.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		for (int i = 0; i < asks.length(); i++) {
			JSONObject jsonobject = asks.getJSONObject(i);
			sellOrders.add(new Order(jsonobject.getDouble("price"), jsonobject
					.getDouble("amount")));
		}
		result.setSellOrders(sellOrders);
		for (int i = 0; i < bids.length(); i++) {
			JSONObject jsonobject = bids.getJSONObject(i);
			buyOrders.add(new Order(jsonobject.getDouble("price"), jsonobject
					.getDouble("amount")));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}
}
