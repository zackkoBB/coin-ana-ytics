package com.gconsulting.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class CAMPBXInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		JSONObject data = source.getJSONObject("pairs");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
//		JSONObject data = source.getJSONObject("btc_usd");
		JSONArray asks = source.getJSONArray("Asks");
		JSONArray bids = source.getJSONArray("Bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		for (int i = 0; i < asks.length(); i++) {
			JSONArray jsonarray = asks.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if(jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();				
			} else {
				price = (Double) jsonarray.get(0);
			}
			if(jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1))).doubleValue();				
			} else {
				amount = (Double) jsonarray.get(1);
			}
			sellOrders.add(new Order(price,amount));
		}
		result.setSellOrders(sellOrders);
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if(jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();				
			} else {
				price = (Double) jsonarray.get(0);
			}
			if(jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1))).doubleValue();				
			} else {
				amount = (Double) jsonarray.get(1);
			}
			buyOrders.add(new Order(price,amount));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}

}
