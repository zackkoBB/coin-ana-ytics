package com.gconsulting.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class BITYESInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
		// JSONObject data = source.getJSONObject("data");
		JSONArray asks = source.getJSONArray("asks");
		JSONArray bids = source.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		for (int i = asks.length() - 1; i >= 0; i--) {
			JSONArray jsonarray = asks.getJSONArray(i);
			sellOrders.add(new Order(new Double((String) jsonarray.get(0)),
					new Double((String) jsonarray.get(1))));
		}
		result.setSellOrders(sellOrders);
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			buyOrders.add(new Order(new Double((String) jsonarray.get(0)),
					new Double((String) jsonarray.get(1))));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}
}
