package com.gconsulting.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class TROCKTInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
		JSONArray asks = source.getJSONArray("asks");
		JSONArray bids = source.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		for (int i = 0; i < asks.length() ; i++) {
			JSONArray jsonarray = asks.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if(jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();				
			} else {
				price = (Double) jsonarray.get(0);
			}
			if(jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1))).doubleValue();				
			} else {
				amount = (Double) jsonarray.get(1);
			}
			sellOrders.add(new Order(price,amount));
		}
		result.setSellOrders(sellOrders);
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if(jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();				
			} else {
				price = (Double) jsonarray.get(0);
			}
			if(jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1))).doubleValue();				
			} else {
				amount = (Double) jsonarray.get(1);
			}
			buyOrders.add(new Order(price,amount));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}
}
