package com.gconsulting.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class KRAKENInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		JSONObject data = source.getJSONObject("pairs");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source) throws JSONException {
		
		return getOrderbook(source, "XXBTZUSD");
	}

	public Orderbook getOrderbook(JSONObject source, String suffix) throws JSONException {
				
		Orderbook result = new Orderbook();
		JSONObject data = source.getJSONObject("result");
		JSONObject returnObject = data.getJSONObject(suffix);
		JSONArray asks = returnObject.getJSONArray("asks");
		JSONArray bids = returnObject.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		for (int i = 0; i < asks.length(); i++) {
			JSONArray jsonarray = asks.getJSONArray(i);
			sellOrders.add(new Order(new Double((String) jsonarray.get(0)),
					new Double((String) jsonarray.get(1))));
		}
		result.setSellOrders(sellOrders);
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			buyOrders.add(new Order(new Double((String) jsonarray.get(0)),
					new Double((String) jsonarray.get(1))));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}
}
