package com.gconsulting.rest.util;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.json.JSONObject;

import com.gconsulting.rest.model.API;

public class RESTUtils {

	/**
	 * Read a GET JSON API
	 * 
	 * @param source
	 *            API for that REST API
	 * @return JSONObject JSON object read
	 */
	public static JSONObject getAPI(API source) {

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI(source.getAddress()));
		Builder builder = target.request(MediaType.APPLICATION_JSON_TYPE);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return new JSONObject(response.readEntity(String.class));
	}

	/**
	 * Read a GET Text API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return JSONObject JSON object read
	 */
	public static String getAPIText(API source) {

		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(getBaseURI(source.getAddress()));
		Builder builder = target.request(MediaType.APPLICATION_XML);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

}
