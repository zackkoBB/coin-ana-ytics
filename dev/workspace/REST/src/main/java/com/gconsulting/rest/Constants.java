package com.gconsulting.rest;

import java.util.ArrayList;
import java.util.List;

public class Constants {

    public static final String MARKET_USD = "BTC_USD";
	public static final String API_TYPE_ORDERBOOK = "ORDERBOOK";
	public static final String FEE_TYPE_TRADE = "TRADE";
    public static final String FEE_TRADE_BTC_USD_1 = "FIAT";
    public static final String FEE_TRADE_BTC_USD_2 = "CRYPTO";
    public static final String REST_BASE_PACKAGE = "com.gconsulting.rest.interfaces";

    /*
    public static final String ALLCOIN_PAIRS_URL = "https://www.allcoin.com/api2/pairs";
	public static final String BTCE_PAIRS_URL = "https://btc-e.com/api/3/info";

	public static final String ANXPRO_BTC_USD_ORDERBOOK__URL = "https://anxpro.com/api/2/BTCUSD/money/depth/full";
	public static final String BITFINEX_BTC_USD_ORDERBOOK__URL = "https://api.bitfinex.com/v1/book/btcusd";
	public static final String BITKONAN_BTC_USD_ORDERBOOK__URL = "https://bitkonan.com/api/btc_orderbook";
	public static final String BITSTAMP_BTC_USD_ORDERBOOK__URL = "https://www.bitstamp.net/api/order_book/";
	public static final String BITYES_BTC_USD_ORDERBOOK__URL = "https://market.bityes.com/usd_btc/depth.js";
	public static final String BTCE_BTC_USD_ORDERBOOK__URL = "https://btc-e.com/api/3/depth/btc_usd";
	public static final String BTER_BTC_USD_ORDERBOOK__URL = "http://data.bter.com/api/1/depth/btc_usd";
	public static final String CAMPBX_BTC_USD_ORDERBOOK__URL = "http://campbx.com/api/xdepth.php";
	public static final String CRYPTOTRADE_BTC_USD_ORDERBOOK__URL = "https://crypto-trade.com/api/1/depth/btc_usd";
	public static final String CRYPTSY_BTC_USD_ORDERBOOK__URL = "http://pubapi2.cryptsy.com/api.php?method=singleorderdata&marketid=2";
	public static final String EXMO_BTC_USD_ORDERBOOK__URL = "https://api.exmo.com/api_v2/orders_book?pair=BTC_USD";
	public static final String HITBTC_BTC_USD_ORDERBOOK__URL = "https://api.hitbtc.com/api/1/public/BTCUSD/orderbook";
	public static final String INDACOIN_BTC_USD_ORDERBOOK__URL = "https://indacoin.com/api/orderbook?pair=btc_usd";
	public static final String KRAKEN_BTC_USD_ORDERBOOK__URL = "https://api.kraken.com/0/public/Depth?pair=XXBTZUSD";
	public static final String LAKEBTC_BTC_USD_ORDERBOOK__URL = "https://www.lakebtc.com/api_v1/bcorderbook";
	public static final String OKCOIN_BTC_USD_ORDERBOOK__URL = "https://www.okcoin.com/api/depth.do?symbol=btc_usd&ok=1";
	public static final String TROCKT_BTC_USD_ORDERBOOK__URL = "https://www.therocktrading.com/api/orderbook/BTCUSD";
	public static final String VIRTEX_BTC_USD_ORDERBOOK__URL = "https://api.virtex.com/v1/market/order-book?market=BTC-USD&limit=0&group=0";

	public static List<String> getBTC_USDOrderbook() {
		
		List<String> result = new ArrayList<>();
		result.add(ANXPRO_BTC_USD_ORDERBOOK__URL);result.add(BITFINEX_BTC_USD_ORDERBOOK__URL);
		result.add(BITKONAN_BTC_USD_ORDERBOOK__URL);result.add(BITSTAMP_BTC_USD_ORDERBOOK__URL);
		result.add(BITYES_BTC_USD_ORDERBOOK__URL);result.add(BTCE_BTC_USD_ORDERBOOK__URL);
		result.add(BTER_BTC_USD_ORDERBOOK__URL);result.add(CAMPBX_BTC_USD_ORDERBOOK__URL);
		result.add(CRYPTOTRADE_BTC_USD_ORDERBOOK__URL);result.add(CRYPTSY_BTC_USD_ORDERBOOK__URL);
		result.add(EXMO_BTC_USD_ORDERBOOK__URL);result.add(HITBTC_BTC_USD_ORDERBOOK__URL);
		result.add(INDACOIN_BTC_USD_ORDERBOOK__URL);result.add(KRAKEN_BTC_USD_ORDERBOOK__URL);
		result.add(LAKEBTC_BTC_USD_ORDERBOOK__URL);result.add(OKCOIN_BTC_USD_ORDERBOOK__URL);
		result.add(TROCKT_BTC_USD_ORDERBOOK__URL);result.add(VIRTEX_BTC_USD_ORDERBOOK__URL);
		return result;
	}*/

	public static List<String> getUSDTradingFees() {
		
		List<String> types = new ArrayList<String>();
		types.add(Constants.FEE_TRADE_BTC_USD_1);
		types.add(Constants.FEE_TRADE_BTC_USD_2);
		return types;
	}
}
