package com.gconsulting.rest;

import it.unimi.dsi.fastutil.BigList;
import it.unimi.dsi.fastutil.objects.ObjectBigArrayBigList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.rest.interfaces.BTCCHARTSInterface;
import com.gconsulting.rest.interfaces.RESTInterface;
import com.gconsulting.rest.model.API;
import com.gconsulting.rest.model.Exchange;
import com.gconsulting.rest.model.Fee;
import com.gconsulting.rest.model.FeeApiType;
import com.gconsulting.rest.model.Market;
import com.gconsulting.rest.model.ids.FeeApiId;
import com.gconsulting.rest.util.RESTUtils;
import com.gconsulting.rest.webapp.model.Data;
import com.gconsulting.rest.webapp.model.Order;
import com.gconsulting.rest.webapp.model.Orderbook;

public class ReadTest {

	private static int LIMIT_VIEW = 10;
	private static Session session;
	private static Map<String, Orderbook> orderbooks;
	private static Map<String, Fee> tradingFees;
	private static Market usdMarket;
	private static Map<String, List<Data>> historicalData;
	private int arbVolume;

	private static Market getMarketByCode(String code) {

		Query qry = session.createQuery("from Market m where m.code='" + code
				+ "'");
		if (qry.list() != null) {
			if (qry.list().size() > 0) {
				return (Market) qry.list().get(0);
			}
		}
		return null;
	}

	private static Exchange getExchangeById(String id) {
		return (Exchange) session.get(Exchange.class, id);
	}

	private static List<Exchange> getAllExchange() {
		return (List<Exchange>) session.createCriteria(Exchange.class).list();
	}

	private static API getApiById(FeeApiId id) {
		return (API) session.get(API.class, id);
	}

	private static List<API> getApiByExchange(Exchange exchange) {

		Query qry = session.createQuery("from API a where a.exchange.code='"
				+ exchange.getCode() + "'");
		return qry.list();
	}

	private static List<Fee> getFeeByExchangeType(Exchange exchange, String type) {

		Query qry = session.createQuery("from Fee f where f.exchange.code='"
				+ exchange.getCode() + "'" + " and f.feeType.type='" + type
				+ "'");
		return qry.list();
	}

	private static void displayOrderbook() {

		int i = 0;
		System.out.println("Displaying data...");
		if (orderbooks != null) {
			for (String exchange : orderbooks.keySet()) {
				i = 0;
				System.out.println("Exchange: " + exchange);
				Fee tradingFee = tradingFees.get(exchange);
				if (tradingFee != null) {
					System.out.println("Trading Fee: " + tradingFee.getValue()
							+ tradingFee.getUnit());
				}
				Orderbook orderbook = orderbooks.get(exchange);
				System.out.print("sellOrder: ");
				for (Order sellOrder : orderbook.getSellOrders()) {
					System.out.print(sellOrder.getPrice() + " "
							+ sellOrder.getQuantity() + " | ");
					if (i++ > LIMIT_VIEW)
						break;
				}
				i = 0;
				System.out.println();
				System.out.print("buyOrder: ");
				for (Order buyOrder : orderbook.getBuyOrders()) {
					System.out.print(buyOrder.getPrice() + " "
							+ buyOrder.getQuantity() + " | ");
					if (i++ > LIMIT_VIEW)
						break;
				}
				System.out.println();
			}
		}
	}

	private static void getOrderbooks() throws ClassNotFoundException,
			InstantiationException, IllegalAccessException {

		int i = 0;
		Transaction tx = session.beginTransaction();
		orderbooks = new TreeMap<String, Orderbook>();
		tradingFees = new HashMap<String, Fee>();
		usdMarket = getMarketByCode(Constants.MARKET_USD);
		List<Exchange> exchanges = getAllExchange();
		List<API> sources = new ArrayList<>();
		API temp = null;
		for (Exchange exchange : exchanges) {
			temp = getApiById(new FeeApiId(exchange, new FeeApiType(
					Constants.API_TYPE_ORDERBOOK, usdMarket)));
			if (temp != null) {
				sources.add(temp);
			}
		}
		System.out.println("Getting data...");
		for (API source : sources) {
			if (source.getExchange().getRestInterface()) {
				try {
					i++;
					System.out.print(i + ") API: " + source.getAddress());
					JSONObject response = RESTUtils.getAPI(source);
					Class<?> restInterfaceClass = Class
							.forName(Constants.REST_BASE_PACKAGE + "."
									+ source.getExchange().getCode()
									+ "Interface");
					RESTInterface restInterface = (RESTInterface) restInterfaceClass
							.newInstance();
					orderbooks.put(source.getExchange().getCode(),
							restInterface.getOrderbook(response));
					List<Fee> tradingFeesForExchange = getFeeByExchangeType(
							source.getExchange(), Constants.FEE_TYPE_TRADE);
					Fee usdFee = null;
					for (Fee tradingFeeForExchange : tradingFeesForExchange) {
						for (String usdTradingFeeMarketCode : Constants
								.getUSDTradingFees()) {
							if (tradingFeeForExchange.getFeeType().getMarket()
									.getCode()
									.equalsIgnoreCase(usdTradingFeeMarketCode)) {
								usdFee = tradingFeeForExchange;
								break;
							}
						}
						if (usdFee != null) {
							break;
						}
					}
					tradingFees.put(source.getExchange().getCode(), usdFee);
					System.out.println(" OK!");
//					System.out.println();
				} catch (JSONException e) {
					System.out.println(" Cannot get the Orderbook: "
							+ source.getExchange().getName() + " "
							+ e.getMessage());
				}
			}
		}
		tx.commit();
	}

	private static void getHistoricalData() throws ClassNotFoundException,
			InstantiationException, IllegalAccessException {

		int i = 0;
		Transaction tx = session.beginTransaction();
		BigList<Data> historicalDataBigList = new ObjectBigArrayBigList<>();
		historicalData = new TreeMap<String, List<Data>>();
		Exchange dataExchange = getExchangeById("BTCCHARTS");
		List<API> sources = getApiByExchange(dataExchange);
		System.out.println("Getting data...");
		for (API source : sources) {
			if (source.getExchange().getRestInterface()) {
					i++;
					System.out.print(i + ") API: " + source.getAddress());
					String response = RESTUtils.getAPIText(source);
					BTCCHARTSInterface restInterface = new BTCCHARTSInterface();
					historicalData.put(source.getFeeType().getMarket().getCode(),
							restInterface.getHistoricalData(response));
					System.out.println(" OK!");
			}
		}
		tx.commit();
	}

	private static void displayHistoricalData() {

		int i = 0;
		System.out.println("Displaying data...");
		if (historicalData != null) {
			for (String market : historicalData.keySet()) {
				i = 0;
				System.out.println("Market: " + market);

				List<Data> dataList= historicalData.get(market);
				System.out.print("Data: ");
				for (Data data : dataList) {
					System.out.print(data.getTime() + " "
							+ data.getPrice() + " " + data.getQuantity() + " | ");
					if (i++ > LIMIT_VIEW)
						break;
				}
				System.out.println();
			}
		}
	}

	public static void main(String[] args) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException {

		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");
		SessionFactory factory = cfg.buildSessionFactory();
		session = factory.openSession();
		getOrderbooks();
		displayOrderbook();
		getHistoricalData();
		displayHistoricalData();
		session.close();
		factory.close();
	}
}
