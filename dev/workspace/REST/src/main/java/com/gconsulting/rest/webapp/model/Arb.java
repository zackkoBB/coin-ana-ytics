package com.gconsulting.rest.webapp.model;

import com.gconsulting.rest.model.Exchange;
import com.gconsulting.rest.model.Market;

public class Arb {

	private Double value;
	private Market market;
	private Exchange startExchange;
	private Exchange endExchange;
	private Double startPrice;
	private Double endPrice;
//	private List<Trade> startTrade;
//	private List<Trade> endTrade;
//	private Trade transferTrade;

	public Arb() {
		super();
	}

	public Arb(Double value, Market market, Exchange startExchange,
			Exchange endExchange, Double startPrice, Double endPrice) {
		super();
		this.value = value;
		this.market = market;
		this.startExchange = startExchange;
		this.endExchange = endExchange;
//		this.startTrade = startTrade;
//		this.endTrade = endTrade;
//		this.transferTrade = transferTrade;
		this.startPrice = startPrice;
		this.endPrice = endPrice;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Market getMarket() {
		return market;
	}

	public void setMarket(Market market) {
		this.market = market;
	}

	public Exchange getStartExchange() {
		return startExchange;
	}

	public void setStartExchange(Exchange startExchange) {
		this.startExchange = startExchange;
	}

	public Exchange getEndExchange() {
		return endExchange;
	}

	public void setEndExchange(Exchange endExchange) {
		this.endExchange = endExchange;
	}
//
//	public List<Trade> getStartTrade() {
//		return startTrade;
//	}
//
//	public void setStartTrade(List<Trade> startTrade) {
//		this.startTrade = startTrade;
//	}
//
//	public List<Trade> getEndTrade() {
//		return endTrade;
//	}
//
//	public void setEndTrade(List<Trade> endTrade) {
//		this.endTrade = endTrade;
//	}
//
//	public Trade getTransferTrade() {
//		return transferTrade;
//	}
//
//	public void setTransferTrade(Trade transferTrade) {
//		this.transferTrade = transferTrade;
//	}

	public Double getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Double startPrice) {
		this.startPrice = startPrice;
	}

	public Double getEndPrice() {
		return endPrice;
	}

	public void setEndPrice(Double endPrice) {
		this.endPrice = endPrice;
	}	
}
