package com.gconsulting.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gconsulting.dao.NewsletterDao;
import com.gconsulting.model.Newsletter;
import com.gconsulting.service.NewsletterManager;

/**
 * Implementation of UserManager interface.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
@Service("newsletterManager")
public class NewsletterManagerImpl extends
		GenericManagerImpl<Newsletter, String> implements NewsletterManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8253446758121224955L;
	private NewsletterDao newsletterDao;

	@Override
	@Autowired
	public void setNewsletterDao(final NewsletterDao newsletterDao) {
		this.dao = newsletterDao;
		this.newsletterDao = newsletterDao;
	}

	/**
	 * {@inheritDoc}
	 */
    public Newsletter getNewsletter(String email) {
		return newsletterDao.loadNewsletterByEmail(email);		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Newsletter> getNewsletters() {
		return newsletterDao.getAllDistinct();		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Newsletter saveNewsletter(Newsletter newsletter) throws Exception {
		
		try {
			return newsletterDao.save(newsletter);
		} catch (final Exception e) {
			e.printStackTrace();
			log.warn(e.getMessage());
			throw new Exception("newsletter '" + newsletter.getEmail()
					+ "' already exists!");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeNewsletter(Newsletter newsletter) {
		log.debug("removing Newsletter: " + newsletter);
		newsletterDao.remove(newsletter);
	}
}
