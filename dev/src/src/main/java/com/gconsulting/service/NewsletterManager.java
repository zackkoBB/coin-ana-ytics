package com.gconsulting.service;

import java.io.Serializable;
import java.util.List;

import com.gconsulting.dao.NewsletterDao;
import com.gconsulting.model.Newsletter;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *  Modified by <a href="mailto:dan@getrolling.com">Dan Kibler </a>
 */
public interface NewsletterManager extends GenericManager<Newsletter, String>, Serializable {
    /**
     * Convenience method for testing - allows you to mock the DAO and set it on an interface.
     * @param userDao the UserDao implementation to use
     */
    void setNewsletterDao(NewsletterDao newsletterDao);

    /**
     * Retrieves a user by userId.  An exception is thrown if user not found
     *
     * @param userId the identifier for the user
     * @return User
     */
    Newsletter getNewsletter(String email);

    /**
     * Retrieves a list of all users.
     * @return List
     */
    List<Newsletter> getNewsletters();

    /**
     * Saves a user's information.
     *
     * @param user the user's information
     * @throws UserExistsException thrown when user already exists
     * @return user the updated user object
     */
    Newsletter saveNewsletter(Newsletter newsletter) throws Exception;

    /**
     * Removes a user from the database
     *
     * @param user the user to remove
     */
    void removeNewsletter(Newsletter newsletter);
}
