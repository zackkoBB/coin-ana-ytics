package com.gconsulting;

import java.util.ArrayList;
import java.util.List;

/**
 * Constant values used throughout the application.
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 */
public final class Constants {

	private Constants() {
		// hide me
	}

	// ~ Static fields/initializers
	// =============================================

	/**
	 * Dashboard
	 */
	public static final int SECOND_TO_MILLISECOND = 1000;
	public static final String MARKET_BTC_USD = "BTC_USD";
	public static final String MARKET_BTC_EUR = "BTC_EUR";
	public static final String MARKET_BTC_CNY = "BTC_CNY";
	public static final String MARKET_BTC_RUB = "BTC_RUB";
	public static final String MARKET_BTC_GBP = "BTC_GBP";
	public static final String MARKET_USD_EUR = "USD_EUR";
	public static final String MARKET_CNY_EUR = "CNY_EUR";
	public static final String MARKET_RUB_EUR = "RUB_EUR";
	public static final String MARKET_GBP_EUR = "GBP_EUR";
	public static final String FEE_TRADE1 = "FIAT";
	public static final String FEE_TRADE2 = "CRYPTO";
	public static final Double FEE_BTC_NETWORK_FEE = 0.0001;
	public static final String FEE_BTC_NETWORK_FEE_NOTE = "Default BTC Network Fee";
	public static final String HISTORICAL_DATA_TYPE = "HISTORICAL_DATA";
	public static final String HISTORICAL_DATA_EXCHANGE = "BTCCHARTS";
	public static final String EXCHANGE_RATE_EXCHANGE = "QUANDL";
	public static final String CROSS_CURRENCY_EXCHANGE_RATE_EXCHANGE = "YAHOO";
	public static final String MARKET_USD = "USD";
	public static final String MARKET_EUR = "EUR";
	public static final String MARKET_CNY = "CNY";
	public static final String MARKET_RUB = "RUB";
	public static final String MARKET_GBP = "GBP";
	/**
	 * ARBS
	 */
	public static final int ARB_BTC_SIZE1 = 1;
	public static final int ARB_BTC_SIZE2 = 10;
	public static final int ARB_BTC_SIZE3 = 50;
	public static final int ARB_BTC_SIZE4 = 100;
	public static final int ARB_BTC_SIZE5 = 200;
	public static final int ARB_BTC_SIZE6 = 500;
	public static final int ARB_BTC_SIZE7 = 1000;
	public static final int REST_CONNECTION_TIMEOUT = 180000;
	public static final int REST_READ_TIMEOUT = 180000;
	public static final int REST_ARBITRAGE_POLLING_INTERVAL = 15000;// 60000;//15000;
	/**
	 * Polling interval for HistoricalData/ExchangeRates:
	 * REST_ARBITRAGE_POLLING_INTERVAL * REST_HISTORICAL_DATA_POLLING_INTERVAL =
	 * (millis) * N
	 */
	public static final int REST_HISTORICALA_DATA_POLLING_INTERVAL = 30;// 1800000;
	/**
	 * Polling interval for HistoricalData/ExchangeRates:
	 * REST_DELAYED_POLLING_INTERVAL * REST_HISTORICAL_DATA_POLLING_INTERVAL =
	 * (millis) * N
	 */
	public static final int REST_DELAYED_DATA_POLLING_INTERVAL = 1440;// 1800000;
	public static final double VOLUME_PRECISION = 0.01;
	public static final int SKIPPED_SUMMARY_LENGTH = 10;
	public static final String CSV_SUFFIX = ".csv";
	public static final String EXCHANGES_IMPORT_HEADER1 = "Code";
	public static final String EXCHANGES_IMPORT_HEADER2 = "Name";
	public static final String EXCHANGES_IMPORT_HEADER3 = "Description";
	public static final String EXCHANGES_IMPORT_HEADER4 = "Website";
	public static final String EXCHANGES_IMPORT_HEADER5 = "Api";
	public static final String EXCHANGES_IMPORT_HEADER6 = "Fee";
	public static final String EXCHANGES_IMPORT_HEADER7 = "Interface";
	public static final String TYPES_IMPORT_HEADER1 = "Market";
	public static final String TYPES_IMPORT_HEADER2 = "Type";
	public static final String APIS_IMPORT_HEADER1 = "Exchange";
	public static final String APIS_IMPORT_HEADER2 = "Market";
	public static final String APIS_IMPORT_HEADER3 = "Type";
	public static final String APIS_IMPORT_HEADER4 = "Address";
	public static final String FEES_IMPORT_HEADER1 = "Exchange";
	public static final String FEES_IMPORT_HEADER2 = "Market";
	public static final String FEES_IMPORT_HEADER3 = "Type";
	public static final String FEES_IMPORT_HEADER4 = "Unit";
	public static final String FEES_IMPORT_HEADER5 = "Value";
	public static final String FEES_IMPORT_HEADER6 = "Note";
	public static final String REST_BASE_PACKAGE = "com.gconsulting.webapp.rest.interfaces";
	public static final String MARKET_CODE_NONE = "NONE";
	public static final String API_TYPE_PAIRS = "PAIRS";
	public static final String API_TYPE_ORDERBOOK = "ORDERBOOK";
	public static final String API_TYPE_TICKER = "TICKER";
	public static final String API_TYPE_FUNDING = "FUNDING";
	public static final String FEE_TYPE_TRADE = "TRADE";
	public static final String FEE_TYPE_DEPOSIT = "DEPOSIT";
	public static final String FEE_TYPE_WITHDRAW = "WITHDRAW";
	public static final String FEE_UNIT_EUR = "€";
	public static final String FEE_UNIT_USD = "$";
	public static final String FEE_UNIT_CNY = "¥";
	public static final String FEE_UNIT_RUB = "₽";
	public static final String FEE_UNIT_GBP = "£";
	public static final String FEE_UNIT_BTC = "BTC";
	public static final String FEE_UNIT_BTCKB = "BTC/kB";
	public static final String FEE_UNIT_PERCENTAGE = "%";
	public static final String DEFAULT_THEME = "bootstrap";
	public static final String[] POSSIBLE_THEMES = { "bootstrap",
			"afterdark", "black-tie", "blitzer", "bluesky", "cruze",
			"cupertino", "dark-hive", "glass-x", "hot-sneaks", "humanity",
			"le-frog", "midnight", "mint-choc", "overcast", "pepper-grinder",
			"redmond", "rocket", "sam", "smoothness", "south-street", "start",
			"sunny", "swanky-purse", "trontastic", "ui-darkness",
			"ui-lightness", "vader" };
	/**
	 * 
	 * CallBack
	 */
	// public static final String SUBSCRIPTION_CALLBACK_URL =
	// "http://79.54.247.231:8080/coin-analytics/rest/subscribe";
	public static final String SUBSCRIPTION_CALLBACK_URL = "http://www.coin-analytics.com/rest/subscribe";
	public static final String SUBSCRIPTION_ROOT = "https://blockchain.info/";
	public static final String SUBSCRIPTION_CALLBACK_SECRET = "1A8JiWcwvpY7tAopUkSnGuEYHmzGYfZPiq";
	public static final String SUBSCRIPTION_DEFAULT_PAYING_ADDRESS = "1CgJ3M9kweRAhpjRWAZYvE3UL8cpTjAox1";
	public static final String SUBSCRIPTION_FREE_CODE = "NO";
	public static final String SUBSCRIPTION_1MONTH_CODE = "1MONTH";
	public static final String SUBSCRIPTION_WALLET_ID = "373feac0-b1da-4b98-a540-724cf82eebdc";
	public static final String SUBSCRIPTION_WALLET_PASSWORD = "D14b0l1kBC";

	public static List<String> getTypes() {

		List<String> types = new ArrayList<>();
		types.add(Constants.API_TYPE_PAIRS);
		types.add(Constants.API_TYPE_ORDERBOOK);
		types.add(Constants.API_TYPE_FUNDING);
		types.add(Constants.API_TYPE_TICKER);
		types.add(Constants.FEE_TYPE_TRADE);
		types.add(Constants.FEE_TYPE_DEPOSIT);
		types.add(Constants.FEE_TYPE_WITHDRAW);
		return types;
	}

	public static List<String> getFeeUnits() {

		List<String> types = new ArrayList<>();
		types.add(Constants.FEE_UNIT_EUR);
		types.add(Constants.FEE_UNIT_USD);
		types.add(Constants.FEE_UNIT_BTC);
		types.add(Constants.FEE_UNIT_BTCKB);
		types.add(Constants.FEE_UNIT_PERCENTAGE);
		return types;
	}

	public static List<String> getCurrencies() {

		List<String> types = new ArrayList<>();
		types.add(Constants.MARKET_USD);
		types.add(Constants.MARKET_EUR);
		types.add(Constants.MARKET_CNY);
		types.add(Constants.MARKET_RUB);
		types.add(Constants.MARKET_GBP);
		return types;
	}

	public static List<String> getTradingFeeMarketCode() {

		List<String> types = new ArrayList<>();
		types.add(Constants.FEE_TRADE1);
		types.add(Constants.FEE_TRADE2);
		return types;
	}

	public static List<Integer> getArbBTCSize() {

		List<Integer> arbVolume = new ArrayList<>();
		arbVolume.add(Constants.ARB_BTC_SIZE1);
		arbVolume.add(Constants.ARB_BTC_SIZE2);
		arbVolume.add(Constants.ARB_BTC_SIZE3);
		arbVolume.add(Constants.ARB_BTC_SIZE4);
		arbVolume.add(Constants.ARB_BTC_SIZE5);
		arbVolume.add(Constants.ARB_BTC_SIZE6);
		arbVolume.add(Constants.ARB_BTC_SIZE7);
		return arbVolume;
	}

	/**
	 * Assets Version constant
	 */
	public static final String ASSETS_VERSION = "assetsVersion";
	/**
	 * The name of the ResourceBundle used in this application
	 */
	public static final String BUNDLE_KEY = "ApplicationResources";

	/**
	 * File separator from System properties
	 */
	public static final String FILE_SEP = System.getProperty("file.separator");

	/**
	 * User home from System properties
	 */
	public static final String USER_HOME = System.getProperty("user.home")
			+ FILE_SEP;

	/**
	 * The name of the configuration hashmap stored in application scope.
	 */
	public static final String CONFIG = "appConfig";

	/**
	 * Session scope attribute that holds the locale set by the user. By setting
	 * this key to the same one that Struts uses, we get synchronization in
	 * Struts w/o having to do extra work or have two session-level variables.
	 */
	public static final String PREFERRED_LOCALE_KEY = "org.apache.struts2.action.LOCALE";

	/**
	 * The request scope attribute under which an editable user form is stored
	 */
	public static final String USER_KEY = "userForm";

	/**
	 * The request scope attribute that holds the user list
	 */
	public static final String USER_LIST = "userList";

	/**
	 * The request scope attribute for indicating a newly-registered user
	 */
	public static final String REGISTERED = "registered";

	/**
	 * The name of the Administrator role, as specified in web.xml
	 */
	public static final String ADMIN_ROLE = "ROLE_ADMIN";

	/**
	 * The name of the User role, as specified in web.xml
	 */
	public static final String USER_ROLE = "ROLE_USER";

	/**
	 * The name of the user's role list, a request-scoped attribute when
	 * adding/editing a user.
	 */
	public static final String USER_ROLES = "userRoles";

	/**
	 * The name of the available roles list, a request-scoped attribute when
	 * adding/editing a user.
	 */
	public static final String AVAILABLE_ROLES = "availableRoles";

	/**
	 * The name of the CSS Theme setting.
	 * 
	 * @deprecated No longer used to set themes.
	 */
	public static final String CSS_THEME = "csstheme";
}
