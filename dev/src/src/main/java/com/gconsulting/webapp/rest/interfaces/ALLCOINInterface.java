package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.Orderbook;

public class ALLCOINInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		// System.out.println("Code: " + source.getInt("code"));
		JSONObject data = source.getJSONObject("data");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			// System.out.println(names.get(i));
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source, String market)  throws JSONException {
		// TODO Auto-generated method stub
		return null;
	}

}
