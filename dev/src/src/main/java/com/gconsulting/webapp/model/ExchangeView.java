package com.gconsulting.webapp.model;

import java.util.List;

import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;

public class ExchangeView {

	private Exchange exchange;
	private List<Fee> tradingFees;
	private List<Fee> depositMethods;
	private List<Fee> withdrawMethods;

	public ExchangeView() {
		super();
	}

	public ExchangeView(Exchange exchange, List<Fee> tradingFees,
			List<Fee> depositMethods, List<Fee> withdrawMethods) {
		super();
		this.exchange = exchange;
		this.tradingFees = tradingFees;
		this.depositMethods = depositMethods;
		this.withdrawMethods = withdrawMethods;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public List<Fee> getTradingFees() {
		return tradingFees;
	}

	public void setTradingFees(List<Fee> tradingFees) {
		this.tradingFees = tradingFees;
	}

	public List<Fee> getDepositMethods() {
		return depositMethods;
	}

	public void setDepositMethods(List<Fee> depositMethods) {
		this.depositMethods = depositMethods;
	}

	public List<Fee> getWithdrawMethods() {
		return withdrawMethods;
	}

	public void setWithdrawMethods(List<Fee> withdrawMethods) {
		this.withdrawMethods = withdrawMethods;
	}

}
