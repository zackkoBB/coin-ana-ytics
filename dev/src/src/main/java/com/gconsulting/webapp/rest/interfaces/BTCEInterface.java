package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.Constants;
import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;

public class BTCEInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {

		List<Market> result = new ArrayList<>();
		JSONObject data = source.getJSONObject("pairs");
		JSONArray names = data.names();
		for (int i = 0; i < names.length(); i++) {
			Market market = new Market(
					(new String((String) names.get(i))).toUpperCase(),
					names.get(i) + " market");
			result.add(market);
		}
		return result;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source, String market)
			throws JSONException {
		return getOrderbookByMarket(source, getMarket(market));
	}

	private String getMarket(String market) {

		if (market.startsWith(Constants.MARKET_BTC_USD)) {
			return "btc_usd";
		} else if (market.startsWith(Constants.MARKET_BTC_EUR)) {
			return "btc_eur";
		} else if (market.startsWith(Constants.MARKET_BTC_RUB)) {
			return "btc_rur";
		} else if (market.startsWith(Constants.MARKET_BTC_GBP)) {
			return "btc_gbp";
		}
		return null;
	}

	public Orderbook getOrderbookByMarket(JSONObject source, String marketCode)
			throws JSONException {

		Orderbook result = new Orderbook();
		JSONObject data = source.getJSONObject(marketCode);
		JSONArray asks = data.getJSONArray("asks");
		JSONArray bids = data.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		double cumValue = 0.0;
		for (int i = 0; i < asks.length(); i++) {
			JSONArray jsonarray = asks.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else {
				amount = (Double) jsonarray.get(1);
			}
			cumValue += price * amount;
			sellOrders.add(new Order(price, amount, cumValue));
		}
		result.setSellOrders(sellOrders);
		cumValue = 0.0;
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else {
				amount = (Double) jsonarray.get(1);
			}
			cumValue += price * amount;
			buyOrders.add(new Order(price, amount, cumValue));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}

}
