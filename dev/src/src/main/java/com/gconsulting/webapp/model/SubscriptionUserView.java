package com.gconsulting.webapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.gconsulting.model.SubscriptionUser;

public class SubscriptionUserView {

	private SubscriptionUser subscriptionUser;

	public SubscriptionUserView() {
		super();
	}

	public SubscriptionUserView(SubscriptionUser subscriptionUser) {
		super();
		this.subscriptionUser = subscriptionUser;
	}

	public SubscriptionUser getSubscriptionUser() {
		return subscriptionUser;
	}

	public void setSubscriptionUser(SubscriptionUser subscriptionUser) {
		this.subscriptionUser = subscriptionUser;
	}

	public String getStartDateFormatted() {
		if (subscriptionUser.getStartDate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"HH:mm dd MMM yyyy");
			return formatter.format(new Date(
					subscriptionUser.getStartDate() * 1000));
		} else {
			return "";
		}
	}

	public String getEndDateFormatted() {
		if (subscriptionUser.getEndDate() != null) {
			SimpleDateFormat formatter = new SimpleDateFormat(
					"HH:mm dd MMM yyyy");
			return formatter.format(new Date(
					subscriptionUser.getEndDate() * 1000));
		} else {
			return "";
		}
	}
}
