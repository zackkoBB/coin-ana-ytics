package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;
import com.gconsulting.model.Role;
import com.gconsulting.model.SubscriptionUser;
import com.gconsulting.model.User;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.UserManager;
import com.gconsulting.webapp.arbitrage.ArbitrageScanner;
import com.gconsulting.webapp.model.Arb;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;
import com.gconsulting.webapp.model.Ticker;

@ViewScoped
@Component("arbitrageCNYEURAction")
public class ArbitrageCNYEUR extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2401121716176456596L;
	private ExchangeManager exchangeManager;
	private Map<String, Orderbook> orderbooksCNY;
	private Map<String, Orderbook> orderbooksEUR;
	private List<Ticker> tickersCNY;
	private List<Ticker> tickersEUR;
	private ExchangeRate exchangeRate;
	private Orderbook selectedOrderbook;
	private Exchange selectedExchange;
	private String selectedCurrencyCode;
	private String selectedCurrencySymbol;
	private Map<String, Fee> tradingFees;
	private Map<String, Fee> transferFees;
	private int arbVolume;
	private List<Arb> arbs;
	private Arb selectedArb;
	private Orderbook selectedArbStartExchangeOrderbook;
	private Orderbook selectedArbEndExchangeOrderbook;
	private User user;
	
	public ArbitrageCNYEUR() {
		super();
		setSortColumn("value"); // sets the default sort column
		arbVolume = Constants.ARB_BTC_SIZE1;
	}

	@PostConstruct
	public void init() {

		ArbitrageScanner.getInstance().setExchangeManager(exchangeManager);
		tradingFees = exchangeManager.getFIATTradingFee(exchangeManager
				.getAllExchange());
		transferFees = exchangeManager.getCRYPTOWithdrawFee(exchangeManager
				.getAllExchange());
		getArbsAll();
		selectArb();
	}

	/**
	 * Method to select an Arb Coming from xml: select in the View Coming from
	 * the Action: init
	 * 
	 */
	public void selectArb() {

		String selectedArbStartExchange = getParameter("selectedArbStartExchange");
		String selectedArbEndExchange = getParameter("selectedArbEndExchange");
		if ((selectedArbStartExchange != null)
				&& (selectedArbEndExchange != null)) {
			for (Arb arb : arbs) {
				if (arb.getStartExchange().getCode()
						.equalsIgnoreCase(selectedArbStartExchange)
						&& arb.getEndExchange().getCode()
								.equalsIgnoreCase(selectedArbEndExchange)) {
					selectedArb = arb;
					break;
				}
			}
		} else {
			/*
			 * I'm coming from inside the Action = Init
			 */
			if (arbs != null) {
				if (arbs.size() > 0)
					selectedArb = arbs.get(0);
				if (selectedArb != null) {
					if (selectedArb.getStartExchange() != null) {
						selectedArbStartExchange = selectedArb
								.getStartExchange().getCode();
					}
					if (selectedArb.getEndExchange() != null) {
						selectedArbEndExchange = selectedArb.getEndExchange()
								.getCode();
					}
				}
			}
		}
		if ((selectedArbStartExchange != null)
				&& (selectedArbEndExchange != null)) {
			selectedArbStartExchangeOrderbook = orderbooksCNY
					.get(selectedArbStartExchange);
			selectedArbEndExchangeOrderbook = orderbooksEUR
					.get(selectedArbEndExchange);
		}
	}

	public void updateArbs() {
		getArbsAll();
		selectArb();
	}

	/**
	 * Check for arbs: call all the REST before
	 * 
	 * @return List<Arb> retrieved
	 */
	private List<Arb> getArbsAll() {

		boolean tickersSubKeysAdded = false;
		arbs = new ArrayList<>();
		tickersCNY = new ArrayList<>();
		tickersEUR = new ArrayList<>();
		if (activeSubscription()) {
			orderbooksCNY = ArbitrageScanner.getInstance().getOrderbooksCNY();
			orderbooksEUR = ArbitrageScanner.getInstance().getOrderbooksEUR();
		} else {
			orderbooksCNY = ArbitrageScanner.getInstance().getOrderbooksCNYDelayed();
			orderbooksEUR = ArbitrageScanner.getInstance().getOrderbooksEURDelayed();
		}
		if (ArbitrageScanner.getInstance().getCrossCurrencyExchangeRate() != null) {
			for (String exchangeRateCode : ArbitrageScanner.getInstance()
					.getCrossCurrencyExchangeRate().keySet()) {
				if (exchangeRateCode.equalsIgnoreCase(Constants.MARKET_CNY_EUR)) {
					exchangeRate = ArbitrageScanner.getInstance()
							.getCrossCurrencyExchangeRate()
							.get(exchangeRateCode);
				}
			}
		}
		if ((orderbooksCNY != null) && (orderbooksEUR != null)) {
			Set<String> referenceKeys = orderbooksCNY.keySet();
			Set<String> compareKeys = orderbooksEUR.keySet();
			Orderbook referenceOrderbook;
			Orderbook compareOrderbook;
			for (String key : referenceKeys) {
				double referenceValue = 0;
				double referenceVolume = 0;
				/*
				 * Fill Tickers CNY
				 */
				if (orderbooksCNY.get(key) != null) {
					if ((orderbooksCNY.get(key).getSellOrders().get(0) != null)
							&& (orderbooksCNY.get(key).getBuyOrders().get(0) != null)) {
						tickersCNY
								.add(new Ticker(exchangeManager
										.getExchangeByCode(key), orderbooksCNY
										.get(key).getSellOrders().get(0),
										orderbooksCNY.get(key).getBuyOrders()
												.get(0), orderbooksCNY.get(key)));
					}
				}
				referenceOrderbook = orderbooksCNY.get(key);
				for (Order order : referenceOrderbook.getSellOrders()) {
					if ((referenceVolume + order.getQuantity()) > arbVolume) {
						/*
						 * Too much: go out
						 */
						// referenceVolume += (arbVolume - referenceVolume);
						referenceValue += order.getPrice()
								* (arbVolume - referenceVolume);
						referenceVolume += (arbVolume - referenceVolume);
						break;
					} else {
						/*
						 * Is fine: sum all the Volume
						 */
						referenceVolume += order.getQuantity();
						referenceValue += order.getPrice()
								* order.getQuantity();
					}
				}
				/*
				 * Start Fee: on the referenceVolume
				 */
				// referenceValue -= (referenceValue / 100.00)
				// * (new Double(tradingFees.get(key).getValue()));
				referenceVolume -= (referenceVolume / 100.00)
						* (new Double(tradingFees.get(key).getValue()));
				double receivedVolume = referenceVolume
						- (referenceVolume / 100.00)
						* (new Double(transferFees.get(key).getValue()));
				for (String subKey : compareKeys) {
					/*
					 * Fill Tickers EUR
					 */
					if (!tickersSubKeysAdded) {
						if (orderbooksEUR.get(subKey) != null) {
							if ((orderbooksEUR.get(subKey).getSellOrders()
									.get(0) != null)
									&& (orderbooksEUR.get(subKey)
											.getBuyOrders().get(0) != null)) {
								tickersEUR.add(new Ticker(exchangeManager
										.getExchangeByCode(subKey),
										orderbooksEUR.get(subKey)
												.getSellOrders().get(0),
										orderbooksEUR.get(subKey)
												.getBuyOrders().get(0),
										orderbooksEUR.get(subKey)));
							}
						}
					}
					compareOrderbook = orderbooksEUR.get(subKey);
					double compareValue = 0;
					double compareVolume = 0;
					for (Order order : compareOrderbook.getBuyOrders()) {
						if ((compareVolume + order.getQuantity()) > receivedVolume) {
							/*
							 * Too much: go out
							 */
							// compareVolume += (arbVolume - compareVolume);
							compareValue += order.getPrice()
									* (receivedVolume - compareVolume);
							compareVolume += (receivedVolume - compareVolume);
							break;
						} else {
							/*
							 * Is fine: sum all the Volume
							 */
							compareVolume += order.getQuantity();
							compareValue += order.getPrice()
									* order.getQuantity();
						}
					}
					/*
					 * End Fee: on the compareValue
					 */
					compareValue -= (compareValue / 100.00)
							* (new Double(tradingFees.get(subKey).getValue()));
					if ((referenceValue > 0) && (compareValue > 0)) {
						if (((referenceValue * exchangeRate.getRate()) < compareValue)
								&& (Math.abs(referenceVolume - compareVolume) <= Constants.VOLUME_PRECISION)) {
							/*
							 * ARB
							 */
							arbs.add(new Arb(
									new Double(
											(compareValue * 100.00 / (referenceValue * exchangeRate
													.getRate())) - 100.00),
									null, exchangeManager
											.getExchangeByCode(key),
									tradingFees.get(key), exchangeManager
											.getExchangeByCode(subKey),
									tradingFees.get(subKey), referenceValue,
									referenceVolume, referenceValue
											/ referenceVolume, compareVolume,
									compareValue, compareValue / compareVolume,
									transferFees.get(key), referenceVolume,
									receivedVolume));
						}
					}
				}
				tickersSubKeysAdded = true;
			}
		}
		Collections.sort(arbs, new Comparator<Arb>() {
			@Override
			public int compare(Arb a1, Arb a2) {
				return (a1.getValue().doubleValue() > a2.getValue()
						.doubleValue()) ? 1 : -1;
			}
		});
		Collections.reverse(arbs);
		return arbs;
	}

	/**
	 * Check for arbs: call the REST interface programatically (need to check if
	 * the REST interface for that Exchange has been called already or not)
	 * 
	 * @return
	 */
	// private List<Arb> getArbsProgramatically() {
	//
	// return null;
	// }

	/**
	 * Check for arbs
	 * 
	 * @return
	 */
	public List<Arb> getArbs() {
		return arbs;
	}

	public Arb getSelectedArb() {
		return selectedArb;
	}

	public void setSelectedArb(Arb selectedArb) {
		this.selectedArb = selectedArb;
	}

	public Orderbook getSelectedArbStartExchangeOrderbook() {
		return selectedArbStartExchangeOrderbook;
	}

	public void setSelectedArbStartExchangeOrderbook(
			Orderbook selectedArbStartExchangeOrderbook) {
		this.selectedArbStartExchangeOrderbook = selectedArbStartExchangeOrderbook;
	}

	public Orderbook getSelectedArbEndExchangeOrderbook() {
		return selectedArbEndExchangeOrderbook;
	}

	public void setSelectedArbEndExchangeOrderbook(
			Orderbook selectedArbEndExchangeOrderbook) {
		this.selectedArbEndExchangeOrderbook = selectedArbEndExchangeOrderbook;
	}

	public void selectOrderbook() {

		String code = new String(getParameter("code"));
		String market = new String(getParameter("market"));
		if (market.equals(Constants.MARKET_CNY)) {
			selectedOrderbook = orderbooksCNY.get(code);
			selectedCurrencyCode = Constants.MARKET_CNY;
			selectedCurrencySymbol = Constants.FEE_UNIT_CNY;
		} else if (market.equals(Constants.MARKET_EUR)) {
			selectedOrderbook = orderbooksEUR.get(code);
			selectedCurrencyCode = Constants.MARKET_EUR;
			selectedCurrencySymbol = Constants.FEE_UNIT_EUR;
		}
		selectedExchange = exchangeManager.getExchangeByCode(code);
	}

	public int getArbVolume() {
		return arbVolume;
	}

	public void setArbVolume(int arbVolume) {
		this.arbVolume = arbVolume;
	}

	public List<Integer> getArbVolumes() {
		return Constants.getArbBTCSize();
	}

	public int getArbsNumber() {

		if (arbs != null) {
			return arbs.size();
		}
		return 0;
	}

	public boolean getArbsDisplay() {

		if (arbs != null) {
			if (arbs.size() > 0) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public List<Ticker> getTickersCNY() {
		return tickersCNY;
	}

	public void setTickersCNY(List<Ticker> tickers) {
		this.tickersCNY = tickers;
	}

	public List<Ticker> getTickersEUR() {
		return tickersEUR;
	}

	public void setTickersEUR(List<Ticker> tickersEUR) {
		this.tickersEUR = tickersEUR;
	}

	public Orderbook getSelectedOrderbook() {
		return selectedOrderbook;
	}

	public void setSelectedOrderbook(Orderbook selectedOrderbook) {
		this.selectedOrderbook = selectedOrderbook;
	}

	public Exchange getSelectedExchange() {
		return selectedExchange;
	}

	public void setSelectedExchange(Exchange selectedExchange) {
		this.selectedExchange = selectedExchange;
	}

	public ExchangeRate getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(ExchangeRate exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getSelectedCurrencyCode() {
		return selectedCurrencyCode;
	}

	public void setSelectedCurrencyCode(String selectedCurrencyCode) {
		this.selectedCurrencyCode = selectedCurrencyCode;
	}

	public String getSelectedCurrencySymbol() {
		return selectedCurrencySymbol;
	}

	public void setSelectedCurrencySymbol(String selectedCurrencySymbol) {
		this.selectedCurrencySymbol = selectedCurrencySymbol;
	}

	/**
	 * Get the current logged in user from DB
	 * 
	 * 
	 */
	private void getUserFromDB() {

		if (getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRemoteUser());
		} else {
			user = null;
		}
	}

	/**
	 * Check active subscriptions for the current user
	 * 
	 * 
	 * @return true if a scubscription is active for the current user
	 */
	private boolean activeSubscription() {

		getUserFromDB();
		if (user != null) {
			for (Role role : user.getRoles()) {
				if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
					return true;
				}
			}
			List<SubscriptionUser> subscriptionUser = exchangeManager
					.getSubscriptionUserByUser(user);
			/*
			 * Check for current active subscription
			 */
			Calendar now = Calendar.getInstance();
			long maxSubscriptionEndDate = now.getTimeInMillis() / 1000;
			for (SubscriptionUser subscription : subscriptionUser) {
				if (subscription.getEndDate() > (maxSubscriptionEndDate)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getDelayed() {
		return !activeSubscription();
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

}
