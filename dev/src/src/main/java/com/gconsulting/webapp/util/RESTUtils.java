package com.gconsulting.webapp.util;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;

import com.gconsulting.Constants;
import com.gconsulting.model.API;

public class RESTUtils {

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final RESTUtils INSTANCE = new RESTUtils();
	private static Client client;

	private RESTUtils() {

		ClientConfig config = new ClientConfig();
		config = config.property(ClientProperties.CONNECT_TIMEOUT, Constants.REST_CONNECTION_TIMEOUT);
		config = config.property(ClientProperties.READ_TIMEOUT, Constants.REST_READ_TIMEOUT);
		client = ClientBuilder.newClient(config);
	}

	public static RESTUtils getInstance() {
		return INSTANCE;
	}

	
	/**
	 * Read a GET JSON API
	 * 
	 * @param source
	 *            API for that Text API
	 * @return String result
	 */
	public static synchronized String getAPI(API source) {

		WebTarget target = client.target(getBaseURI(source.getAddress()));
		Builder builder = target.request(MediaType.APPLICATION_JSON);
		Response response = builder.get();
		if (response.getStatus() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		return response.readEntity(String.class);
	}

	private static URI getBaseURI(String url) {
		return UriBuilder.fromUri(url).build();
	}

}
