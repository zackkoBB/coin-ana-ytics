package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.webapp.model.ExchangeRate;

public class YAHOOInterface implements ExchangeRateRESTInterface {

	@Override
	public List<ExchangeRate> getExchangeRate(JSONObject source)
			throws JSONException {

		List<ExchangeRate> result = new ArrayList<>();
		JSONObject query = source.getJSONObject("query");
		JSONObject results = query.getJSONObject("results");
		JSONObject rate = results.getJSONObject("rate");
		result.add(new ExchangeRate(rate.getString("Date"), rate.getDouble("Rate")));
		return result;
	}

}
