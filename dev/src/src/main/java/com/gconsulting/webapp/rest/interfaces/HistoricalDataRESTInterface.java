package com.gconsulting.webapp.rest.interfaces;

import java.util.List;

import org.json.JSONException;

import com.gconsulting.webapp.model.Data;

public interface HistoricalDataRESTInterface {

	public List<Data> getHistoricalData(String data) throws JSONException;
}
