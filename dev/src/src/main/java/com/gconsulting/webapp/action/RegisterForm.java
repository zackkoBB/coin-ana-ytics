package com.gconsulting.webapp.action;

import info.blockchain.api.wallet.Address;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.API;
import com.gconsulting.model.Newsletter;
import com.gconsulting.model.Subscription;
import com.gconsulting.model.SubscriptionUser;
import com.gconsulting.model.User;
import com.gconsulting.model.UserAddresses;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.NewsletterManager;
import com.gconsulting.service.RoleManager;
import com.gconsulting.service.UserExistsException;
import com.gconsulting.webapp.exception.ApplicationException;
import com.gconsulting.webapp.util.RESTUtils;

/**
 * JSF Page class to handle signing up a new user.
 *
 * @author mraible
 */
@Scope("request")
@Component("registerForm")
public class RegisterForm extends BasePage implements Serializable {

	private static final long serialVersionUID = 3524937486662786265L;
	private User user = new User();
	private RoleManager roleManager;
	private ExchangeManager exchangeManager;
	private NewsletterManager newsletterManager;

	public String[] getThemes() {
		return (Constants.POSSIBLE_THEMES);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Autowired
	public void setRoleManager(@Qualifier("roleManager") RoleManager roleManager) {
		this.roleManager = roleManager;
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	@Autowired
	public void setNewsletterManager(
			@Qualifier("newsletterManager") NewsletterManager newsletterManager) {
		this.newsletterManager = newsletterManager;
	}

	/**
	 * Generate a Unique payment address for a User to send payment to
	 * https://blockchain.info/api/blockchain_wallet_api
	 * 
	 * @param params
	 *            Extra parameters to be passed to the callback URL
	 * @return Address
	 * @throws Exception
	 */
	private Address generatePaymentAddressWallet(Map<String, String> params)
			throws Exception {

		String url = Constants.SUBSCRIPTION_ROOT + "/merchant/"
				+ Constants.SUBSCRIPTION_WALLET_ID + "/new_address?";
		for (Map.Entry<String, String> param : params.entrySet()) {
			url += param.getKey() + "="
					+ URLEncoder.encode(param.getValue(), "UTF-8") + "&";
		}
		log.info("Calling Blockchain... " + url);
		String response = RESTUtils.getAPI(new API(null, null, url));
		if (response == null)
			throw new Exception("Server Returned NULL Response");
		JSONObject responseJSON = new JSONObject(response);
		if (responseJSON.has("error"))
			throw new Exception((String) responseJSON.getString("error"));
		return new Address(0L, responseJSON.getString("address"),
				responseJSON.getString("label"), 0L);
	}

	public String save() throws Exception {

		user.setEnabled(true);
		// Set the default user role on this new user
		user.addRole(roleManager.getRole(Constants.USER_ROLE));
		// check theme
//		if (user.getTheme().equalsIgnoreCase("default")) {
//			user.setTheme(Constants.DEFAULT_THEME);
//		}
		try {
			user = userManager.saveUser(user);
		} catch (UserExistsException e) {
			addFacesError("errors.existing.user",
					new Object[] { user.getUsername(), user.getEmail() });
			// redisplay the unencrypted passwords
			user.setPassword(user.getConfirmPassword());
			return null;
		}
		try {
			/*
			 * Try to get an address from the Blockchain if not: doesn't matter
			 */
			Map<String, String> params = new HashMap<>();
			params.put("password", Constants.SUBSCRIPTION_WALLET_PASSWORD);
			params.put("second_password", "");
			params.put("label", user.getUsername());
			Address response = generatePaymentAddressWallet(params);
			log.info("Response: " + response.getAddress() + " "
					+ response.getLabel());
			exchangeManager.create(new UserAddresses(user, response
					.getAddress(), Constants.SUBSCRIPTION_CALLBACK_SECRET,
					response.getAddress(), 0,
					Constants.SUBSCRIPTION_CALLBACK_URL));
		} catch (ApplicationException e) {
			log.error("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		/*
		 * Check if user is in the newsletter if is: give 1MONTH subscription
		 */
		List<Newsletter> newsletter = newsletterManager.getNewsletters();
		for (Newsletter email : newsletter) {
			if (email.getEmail().equalsIgnoreCase(user.getEmail())) {
				long now = Calendar.getInstance().getTimeInMillis() / 1000;
				Subscription freeSubscription = exchangeManager
						.getSubscriptionByCode(Constants.SUBSCRIPTION_1MONTH_CODE);
				exchangeManager.create(new SubscriptionUser(user,
						freeSubscription, now + freeSubscription.getDuration(),
						"", now));
			}
		}
		addFacesMessage("user.registered");
		getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE);
		// log user in automatically
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getConfirmPassword(),
				user.getAuthorities());
		auth.setDetails(user);
		SecurityContextHolder.getContext().setAuthentication(auth);
		// Send an account information e-mail
		/*
		 * TODO: fix this
		 */
		// message.setSubject(getText("register.email.subject"));
		// try {
		// sendUserMessage(user, getText("register.email.message"),
		// RequestUtil.getAppURL(getRequest()));
		// } catch (MailException me) {
		// addFacesError(me.getMostSpecificCause().getMessage());
		// return null;
		// }
		return "home";
	}

	public List<String> getCurrencies() {
		return Constants.getCurrencies();
	}
}
