package com.gconsulting.webapp.model;

public class ExchangeRate {

	private String time;
	private Double rate;

	public ExchangeRate() {
		super();
	}

	public ExchangeRate(String time, Double rate) {
		super();
		this.time = time;
		this.rate = rate;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}
