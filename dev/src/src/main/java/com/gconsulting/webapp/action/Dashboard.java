package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.User;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.arbitrage.ArbitrageScanner;
import com.gconsulting.webapp.exception.ApplicationException;
import com.gconsulting.webapp.model.Data;
import com.gconsulting.webapp.model.ExchangeRate;
import com.gconsulting.webapp.model.ExchangeRateView;

@Scope("session")
@Component("dashboardAction")
public class Dashboard extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -426134230267830853L;
	private ExchangeManager exchangeManager;
	private LineChartModel prices;
	private Map<String, List<ExchangeRate>> exchangeRatesMap;
	private List<ExchangeRateView> exchangeRates;
	private String currentMarket;
	private String market1;
	private String market2;
	private String market3;
	private String market4;
	private String market5;
	private String labelMarket1;
	private String labelMarket2;
	private String labelMarket3;
	private String labelMarket4;
	private String labelMarket5;
	private Double max;
	private Double min;
	private Long zoom;
	private Long sampleInterval;
	private Boolean zoom0Enabled;
	private Boolean zoom1Enabled;
	private Boolean zoom2Enabled;
	private Boolean zoom3Enabled;
	private String id;
	private User user;

	public Dashboard() {
		super();
	}

	@PostConstruct
	public void init() throws ApplicationException {

		ArbitrageScanner.getInstance().setExchangeManager(exchangeManager);
		retrieveUser();
		initCharts();
		initExchangeRates();
	}

	private void retrieveUser() {

		// if a user's id is passed in
		if (id != null) {
			log.debug("Editing user, id is: " + id);
			// lookup the user using that id
			user = userManager.getUser(id);
		} else {
			if (getRemoteUser() != null) {
				user = userManager.getUserByUsername(getRemoteUser());
			} else {
				user = null;
			}
		}
	}

	private void initExchangeRates() throws ApplicationException {

		if (user != null) {
			if (user.getCurrency() != null) {
				if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_USD)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateUSD();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_EUR)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateEUR();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_CNY)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateCNY();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_RUB)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateRUB();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_GBP)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateGBP();
				}
			} else {
				throw new ApplicationException("Currency is null!");
			}
		} else {
			throw new ApplicationException("User is null!");
		}
	}
	

	private void retrieveExchangeRates() {

		if (user != null) {
			if (user.getCurrency() != null) {
				if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_USD)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateUSD();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_EUR)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateEUR();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_CNY)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateCNY();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_RUB)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateRUB();
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_GBP)) {
					exchangeRatesMap = ArbitrageScanner.getInstance()
							.getExchangeRateGBP();
				}
			}
		}
	}	

	private void initCharts() throws ApplicationException {

		if (user != null) {
			if (user.getCurrency() != null) {
				if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_USD)) {
					market1 = new String(Constants.MARKET_USD);
					labelMarket1 = getText("home.leftMenu.dollar");
					market2 = new String(Constants.MARKET_EUR);
					labelMarket2 = getText("home.leftMenu.euro");
					market3 = new String(Constants.MARKET_CNY);
					labelMarket3 = getText("home.leftMenu.renminbi");
					market4 = new String(Constants.MARKET_RUB);
					labelMarket4 = getText("home.leftMenu.ruble");
					market5 = new String(Constants.MARKET_GBP);
					labelMarket5 = getText("home.leftMenu.pound");
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_EUR)) {
					market1 = new String(Constants.MARKET_EUR);
					labelMarket1 = getText("home.leftMenu.euro");
					market2 = new String(Constants.MARKET_USD);
					labelMarket2 = getText("home.leftMenu.dollar");
					market3 = new String(Constants.MARKET_CNY);
					labelMarket3 = getText("home.leftMenu.renminbi");
					market4 = new String(Constants.MARKET_RUB);
					labelMarket4 = getText("home.leftMenu.ruble");
					market5 = new String(Constants.MARKET_GBP);
					labelMarket5 = getText("home.leftMenu.pound");
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_CNY)) {
					market1 = new String(Constants.MARKET_CNY);
					labelMarket1 = getText("home.leftMenu.renminbi");
					market2 = new String(Constants.MARKET_USD);
					labelMarket2 = getText("home.leftMenu.dollar");
					market3 = new String(Constants.MARKET_EUR);
					labelMarket3 = getText("home.leftMenu.euro");
					market4 = new String(Constants.MARKET_RUB);
					labelMarket4 = getText("home.leftMenu.ruble");
					market5 = new String(Constants.MARKET_GBP);
					labelMarket5 = getText("home.leftMenu.pound");
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_RUB)) {
					market1 = new String(Constants.MARKET_RUB);
					labelMarket1 = getText("home.leftMenu.ruble");
					market2 = new String(Constants.MARKET_USD);
					labelMarket2 = getText("home.leftMenu.dollar");
					market3 = new String(Constants.MARKET_EUR);
					labelMarket3 = getText("home.leftMenu.euro");
					market4 = new String(Constants.MARKET_CNY);
					labelMarket4 = getText("home.leftMenu.renminbi");
					market5 = new String(Constants.MARKET_GBP);
					labelMarket5 = getText("home.leftMenu.pound");
				} else if (user.getCurrency().equalsIgnoreCase(
						Constants.MARKET_GBP)) {
					market1 = new String(Constants.MARKET_GBP);
					labelMarket1 = getText("home.leftMenu.pound");
					market2 = new String(Constants.MARKET_USD);
					labelMarket2 = getText("home.leftMenu.dollar");
					market3 = new String(Constants.MARKET_EUR);
					labelMarket3 = getText("home.leftMenu.euro");
					market4 = new String(Constants.MARKET_CNY);
					labelMarket4 = getText("home.leftMenu.renminbi");
					market5 = new String(Constants.MARKET_RUB);
					labelMarket5 = getText("home.leftMenu.ruble");
				}
				selectMarket1();
			} else {
				throw new ApplicationException("Currency is null!");
			}
		} else {
			throw new ApplicationException("User is null!");
		}
	}

	/**
	 * Sample data to make more viewable sample rate = 60 minutes (rate = 10
	 * minutes) Method: weighted arithmetic mean P = price Q = quantity
	 * 
	 * A = (P1*Q1 + P2Q2 + ... + PnQn) / (Q1 + Q2 + ... + Qn)
	 * 
	 * Sample
	 */
	private Map<String, List<Data>> sample(Long start, Long end,
			Long sampleIntervalLocal,
			Map<String, List<Data>> historicalDataAll) {

		Long currentTimestamp;
		Double partialSumNom = 0.0;
		Double partialSumDen = 0.0;
		Long iterator;
		List<Data> resultTemp;
		Map<String, List<Data>> result = new TreeMap<String, List<Data>>();
		if (historicalDataAll != null) {
			for (String market : historicalDataAll.keySet()) {
				partialSumNom = 0.0;
				partialSumDen = 0.0;
				iterator = end;
				resultTemp = new ArrayList<>();
				for (Data data : historicalDataAll.get(market)) {
					currentTimestamp = data.getTime();
					/*
					 * Check if interval is at the end
					 */
					partialSumNom += data.getPrice() * data.getQuantity();
					partialSumDen += data.getQuantity();
					if (currentTimestamp < iterator) {
						resultTemp.add(new Data(iterator, partialSumNom
								/ partialSumDen, partialSumDen));
						partialSumNom = 0.0;
						partialSumDen = 0.0;
						iterator -= sampleIntervalLocal;
					} else {
					}
					/*
					 * Out condition
					 */
					if (currentTimestamp < (start - 60000)) {
						break;
					}
				}
				result.put(market, resultTemp);
			}
		}
		return result;
	}

	private void setChartLabels() {

		if (currentMarket.startsWith(Constants.MARKET_USD)) {
			prices.setTitle(getText("home.rightSection.dollar.title"));
			prices.getAxis(AxisType.X).setLabel(
					getText("home.rightSection.dollar.xlabel"));
			prices.getAxis(AxisType.Y).setLabel(
					getText("home.rightSection.dollar.ylabel"));
		} else if (currentMarket
				.startsWith(Constants.MARKET_EUR)) {
			prices.setTitle(getText("home.rightSection.euro.title"));
			prices.getAxis(AxisType.X).setLabel(
					getText("home.rightSection.euro.xlabel"));
			prices.getAxis(AxisType.Y).setLabel(
					getText("home.rightSection.euro.ylabel"));
		} else if (currentMarket
				.startsWith(Constants.MARKET_CNY)) {
			prices.setTitle(getText("home.rightSection.renminbi.title"));
			prices.getAxis(AxisType.X).setLabel(
					getText("home.rightSection.renminbi.xlabel"));
			prices.getAxis(AxisType.Y).setLabel(
					getText("home.rightSection.renminbi.ylabel"));
		} else if (currentMarket
				.startsWith(Constants.MARKET_RUB)) {
			prices.setTitle(getText("home.rightSection.ruble.title"));
			prices.getAxis(AxisType.X).setLabel(
					getText("home.rightSection.ruble.xlabel"));
			prices.getAxis(AxisType.Y).setLabel(
					getText("home.rightSection.ruble.ylabel"));
		} else if (currentMarket
				.startsWith(Constants.MARKET_GBP)) {
			prices.setTitle(getText("home.rightSection.pound.title"));
			prices.getAxis(AxisType.X).setLabel(
					getText("home.rightSection.pound.xlabel"));
			prices.getAxis(AxisType.Y).setLabel(
					getText("home.rightSection.pound.ylabel"));
		}
	}

	private void createLineModels() {

		prices = initLinearModel();
		setChartLabels();
		prices.setLegendPosition("ne");
		prices.setZoom(true);
		Axis yAxis = prices.getAxis(AxisType.Y);
		Double margin = (max - min) / 5.0;
		yAxis.setMin(min - margin);
		yAxis.setMax(max + margin);
		yAxis.setTickFormat("%#.02f");
		DateAxis xAxis = new DateAxis(
				getText("home.rightSection.dollar.xlabel"));
		Long maxX = Calendar.getInstance().getTimeInMillis();
		Long minX = maxX;
		minX -= zoom * Constants.SECOND_TO_MILLISECOND;
		xAxis.setMax(maxX);
		xAxis.setMin(minX);
		xAxis.setTickAngle(-50);
		xAxis.setTickFormat("%b %#d, %H:%M");
		prices.getAxes().put(AxisType.X, xAxis);
	}

	private void createExchangeRates() {

		exchangeRates = new ArrayList<>();
		retrieveExchangeRates();
		if (exchangeRatesMap != null) {
			for (String exchangeRate : exchangeRatesMap.keySet()) {
				List<ExchangeRate> exchangeRateData = exchangeRatesMap
						.get(exchangeRate);
				if (exchangeRateData != null) {
					if (exchangeRateData.size() > 1) {
						String colour = "#808080";
						if (exchangeRateData.get(0).getRate() > exchangeRateData
								.get(1).getRate()) {
							colour = "#00FF00";
						} else if (exchangeRateData.get(0).getRate() < exchangeRateData
								.get(1).getRate()) {
							colour = "#FF0000";
						} else {
							colour = "#808080";
						}
						exchangeRates.add(new ExchangeRateView(exchangeRate
								.replace("_", ""), exchangeRateData.get(0)
								.getRate(), colour));
					}
				}
			}
		}
	}

	private Map<String, List<Data>> getHistoricalData() {

		if (currentMarket.startsWith(Constants.MARKET_USD)) {
			return ArbitrageScanner.getInstance().getHistoricalDataUSD();
		} else if (currentMarket
				.startsWith(Constants.MARKET_EUR)) {
			return ArbitrageScanner.getInstance().getHistoricalDataEUR();
		} else if (currentMarket
				.startsWith(Constants.MARKET_CNY)) {
			return ArbitrageScanner.getInstance().getHistoricalDataCNY();
		} else if (currentMarket
				.startsWith(Constants.MARKET_RUB)) {
			return ArbitrageScanner.getInstance().getHistoricalDataRUB();
		} else if (currentMarket
				.startsWith(Constants.MARKET_GBP)) {
			return ArbitrageScanner.getInstance().getHistoricalDataGBP();
		}
		return null;
	}

	/**
	 * Create USD chart default zoom = 0 (1D)
	 * 
	 * @return
	 */
	private LineChartModel initLinearModel() {

		min = max = 0.0;
		/*
		 * Set as default to 6h ago (zoom = 6h => zoom = 0);
		 */
		Long upperLimit = Calendar.getInstance().getTimeInMillis()
				/ Constants.SECOND_TO_MILLISECOND;
		Long lowerLimit = upperLimit - zoom;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		LineChartModel model = new LineChartModel();
		Map<String, List<Data>> historicalData = sample(lowerLimit,
				upperLimit, sampleInterval, getHistoricalData());
		if (historicalData != null) {
			LineChartSeries series;
			for (String market : historicalData.keySet()) {
				series = new LineChartSeries();
				series.setLabel(exchangeManager.getMarketByCode(market)
						.getDescription());
				List<Data> dataList = historicalData.get(market);
				for (Data data : dataList) {
					if (data.getPrice() > max) {
						max = data.getPrice();
						if (min == 0.0) {
							min = max;
						}
					}
					if (data.getPrice() < min) {
						min = data.getPrice();
					}
					Calendar dataDate = Calendar.getInstance();
					dataDate.setTimeInMillis(data.getTime()
							* Constants.SECOND_TO_MILLISECOND);
					series.set(formatter.format(dataDate.getTime()).toString(),
							data.getPrice());
				}
				model.addSeries(series);
			}
		}
		return model;
	}

	public void selectMarket1() {
		selectCurrentMarket(market1);
	}

	public void selectMarket2() {
		selectCurrentMarket(market2);
	}

	public void selectMarket3() {
		selectCurrentMarket(market3);
	}

	public void selectMarket4() {
		selectCurrentMarket(market4);
	}

	public void selectMarket5() {
		selectCurrentMarket(market5);
	}

	private void selectCurrentMarket(String selectedCurrentMarket) {

		if (selectedCurrentMarket
				.startsWith(Constants.MARKET_USD)) {
			setCurrentMarketUSD();
		} else if (selectedCurrentMarket
				.startsWith(Constants.MARKET_EUR)) {
			setCurrentMarketEUR();
		} else if (selectedCurrentMarket
				.startsWith(Constants.MARKET_CNY)) {
			setCurrentMarketCNY();
		} else if (selectedCurrentMarket
				.startsWith(Constants.MARKET_RUB)) {
			setCurrentMarketRUB();
		} else if (selectedCurrentMarket
				.startsWith(Constants.MARKET_GBP)) {
			setCurrentMarketGBP();
		}
		updateZoom1();
	}

	public void setCurrentMarketUSD() {
		currentMarket = new String(Constants.MARKET_USD);
	}

	public void setCurrentMarketEUR() {
		currentMarket = new String(Constants.MARKET_EUR);
	}

	public void setCurrentMarketCNY() {
		currentMarket = new String(Constants.MARKET_CNY);
	}

	public void setCurrentMarketRUB() {
		currentMarket = new String(Constants.MARKET_RUB);
	}

	public void setCurrentMarketGBP() {
		currentMarket = new String(Constants.MARKET_GBP);
	}

	public void updateZoom0() {

		zoom0Enabled = false;
		zoom1Enabled = true;
		zoom2Enabled = true;
		zoom3Enabled = true;
		zoom = 900L;
		sampleInterval = 30L;
	}

	public void updateZoom1() {

		zoom0Enabled = true;
		zoom1Enabled = false;
		zoom2Enabled = true;
		zoom3Enabled = true;
		zoom = 3600L;
		sampleInterval = 180L;
	}

	public void updateZoom2() {

		zoom0Enabled = true;
		zoom1Enabled = true;
		zoom2Enabled = false;
		zoom3Enabled = true;
		zoom = 21600L;
		sampleInterval = 600L;
	}

	public void updateZoom3() {

		zoom0Enabled = true;
		zoom1Enabled = true;
		zoom2Enabled = true;
		zoom3Enabled = false;
		zoom = 86400L;
		sampleInterval = 3600L;
	}

	public Long getZoom() {
		return zoom;
	}

	public void setZoom(Long zoom) {
		this.zoom = zoom;
	}

	public LineChartModel getPrices() {
		createLineModels();
		return prices;
	}

	public void setPrices(LineChartModel prices) {
		this.prices = prices;
	}

	public List<ExchangeRateView> getExchangeRates() {
		createExchangeRates();
		return exchangeRates;
	}

	public void setExchangeRates(List<ExchangeRateView> exchangeRates) {
		this.exchangeRates = exchangeRates;
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public Boolean getZoom0Enabled() {
		return zoom0Enabled;
	}

	public void setZoom0Enabled(Boolean zoom0Enabled) {
		this.zoom0Enabled = zoom0Enabled;
	}

	public Boolean getZoom1Enabled() {
		return zoom1Enabled;
	}

	public void setZoom1Enabled(Boolean zoom1Enabled) {
		this.zoom1Enabled = zoom1Enabled;
	}

	public Boolean getZoom2Enabled() {
		return zoom2Enabled;
	}

	public void setZoom2Enabled(Boolean zoom2Enabled) {
		this.zoom2Enabled = zoom2Enabled;
	}

	public Boolean getZoom3Enabled() {
		return zoom3Enabled;
	}

	public void setZoom3Enabled(Boolean zoom3Enabled) {
		this.zoom3Enabled = zoom3Enabled;
	}

	public String getLabelMarket1() {
		return labelMarket1;
	}

	public void setLabelMarket1(String labelMarket1) {
		this.labelMarket1 = labelMarket1;
	}

	public String getLabelMarket2() {
		return labelMarket2;
	}

	public void setLabelMarket2(String labelMarket2) {
		this.labelMarket2 = labelMarket2;
	}

	public String getLabelMarket3() {
		return labelMarket3;
	}

	public void setLabelMarket3(String labelMarket3) {
		this.labelMarket3 = labelMarket3;
	}

	public String getLabelMarket4() {
		return labelMarket4;
	}

	public void setLabelMarket4(String labelMarket4) {
		this.labelMarket4 = labelMarket4;
	}

	public String getLabelMarket5() {
		return labelMarket5;
	}

	public void setLabelMarket5(String labelMarket5) {
		this.labelMarket5 = labelMarket5;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
