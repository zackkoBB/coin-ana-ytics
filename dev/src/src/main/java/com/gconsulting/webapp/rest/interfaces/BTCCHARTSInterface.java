package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.JSONException;

import com.gconsulting.webapp.model.Data;

public class BTCCHARTSInterface implements HistoricalDataRESTInterface {

	public List<Data> getHistoricalData(String data) throws JSONException {

		List<Data> result = new ArrayList<>();
		Scanner scanner = new Scanner(data);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			String[] lineElements = line.split(",");
			result.add(new Data(new Long(lineElements[0]), new Double(
					lineElements[1]), new Double(lineElements[2])));
		}
		scanner.close();
		return result;
	}

}
