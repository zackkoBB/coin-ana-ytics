package com.gconsulting.webapp.action;

import info.blockchain.api.receive.ReceiveResponse;
import info.blockchain.api.wallet.Address;

import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.API;
import com.gconsulting.model.Subscription;
import com.gconsulting.model.SubscriptionUser;
import com.gconsulting.model.User;
import com.gconsulting.model.UserAddresses;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.UserManager;
import com.gconsulting.webapp.model.SubscriptionUserView;
import com.gconsulting.webapp.util.RESTUtils;

@Scope("request")
@Component("subscriptionAction")
public class SubscriptionAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7166591922631238778L;
	private ExchangeManager exchangeManager;
	private List<SubscriptionUserView> paymentsHistory;
	private List<UserAddresses> addresses;
	private UserAddresses currentAddress;
	private SubscriptionUserView currentSubscription;
	private Subscription subscription1;
	private Subscription subscription2;
	private Subscription subscription3;
	private Subscription subscription4;
	private Subscription subscription5;
	private String selectedSubscription;
	private Boolean displaySubscriptions;
	private User user;

	public SubscriptionAction() {
		super();
	}

	@PostConstruct
	public void init() {

		getUserFromDB();
		List<Subscription> subscriptions = exchangeManager.getAllSubscription();
		if (subscriptions != null) {
			if (subscriptions.size() > 0) {
				Collections.sort(subscriptions, new Comparator<Subscription>() {
					@Override
					public int compare(Subscription s1, Subscription s2) {
						return (s1.getPrice() > s2.getPrice()) ? 1 : -1;
					}
				});
				// Collections.reverse(subscriptions);
				subscription1 = subscriptions.get(1);
				subscription2 = subscriptions.get(2);
				subscription3 = subscriptions.get(3);
				subscription4 = subscriptions.get(4);
				subscription5 = subscriptions.get(5);
			}
		}
		if (user != null) {
			List<SubscriptionUser> paymentsHistoryTemp = exchangeManager
					.getSubscriptionUserByUser(user);
			paymentsHistory = new ArrayList<>();
			for (SubscriptionUser subscriptionUser : paymentsHistoryTemp) {
				paymentsHistory.add(new SubscriptionUserView(subscriptionUser));
			}
			/*
			 * Check for current active subscription
			 */
			Calendar now = Calendar.getInstance();
			long maxSubscriptionEndDate = now.getTimeInMillis() / 1000;
			for (SubscriptionUserView subscription : paymentsHistory) {
				if (subscription.getSubscriptionUser().getEndDate() > (maxSubscriptionEndDate)) {
					currentSubscription = subscription;
					maxSubscriptionEndDate = currentSubscription
							.getSubscriptionUser().getEndDate();
				}
			}
			if (currentSubscription == null) {
				currentSubscription = new SubscriptionUserView(
						new SubscriptionUser(
								user,
								exchangeManager
										.getSubscriptionByCode(Constants.SUBSCRIPTION_FREE_CODE),
								null, "", null));
				displaySubscriptions = true;
			} else {
				displaySubscriptions = false;
			}
			addresses = exchangeManager.getUserAddressesByUser(user);
			if (addresses != null) {
				if (addresses.size() > 0) {
					currentAddress = addresses.get(0);
				}
			}
			if (currentAddress == null) {
				// currentAddress = new UserAddresses(user,
				// "Please select a Plan",
				// "");
			}
		}
	}

	/**
	 * Generate a Unique payment address for a User to send payment to
	 * https://blockchain.info/api/api_receive
	 * 
	 * @param myAddress
	 *            Your bitcoin address
	 * @param callback
	 *            The callback URL which will be notified when a payment is
	 *            received
	 * @param anonymous
	 *            Whether the transaction should be anonymous or not
	 * @param params
	 *            Extra parameters to be passed to the callback URL
	 * @return
	 * @throws Exception
	 */
	private ReceiveResponse generatePaymentAddressNoWallet(String myAddress,
			String callback, boolean anonymous, Map<String, String> params)
			throws Exception {

		String url = Constants.SUBSCRIPTION_ROOT
				+ "/api/receive?method=create&callback="
				+ URLEncoder.encode(callback, "UTF-8") + "&anonymous="
				+ anonymous + "&address=" + myAddress;
		// Append any custom parameters to the callback
		for (Map.Entry<String, String> param : params.entrySet()) {
			url += "&" + param.getKey() + "="
					+ URLEncoder.encode(param.getValue(), "UTF-8");
		}
		log.info("Calling Blockchain... " + url);
		String response = RESTUtils.getAPI(new API(null, null, url));
		if (response == null)
			throw new Exception("Server Returned NULL Response");
		JSONObject responseJSON = new JSONObject(response);
		if (responseJSON.has("error"))
			throw new Exception((String) responseJSON.getString("error"));
		return new ReceiveResponse(responseJSON.getInt("fee_percent"),
				responseJSON.getString("destination"),
				responseJSON.getString("input_address"),
				responseJSON.getString("callback_url"));
	}

	public void subscribeNoWallet() {

		if (currentAddress == null) {
			/*
			 * Get Current Address from Blockchain
			 */
			Map<String, String> params = new HashMap<>();
			params.put("user", user.getId().toString());
			params.put("secret", Constants.SUBSCRIPTION_CALLBACK_SECRET);
			ReceiveResponse response;
			try {
				response = generatePaymentAddressNoWallet(
						Constants.SUBSCRIPTION_DEFAULT_PAYING_ADDRESS,
						Constants.SUBSCRIPTION_CALLBACK_URL, false, params);
				log.info("Response: " + response.getInputAddress() + " "
						+ response.getDestinationAddress() + " "
						+ response.getFeePercent() + " "
						+ response.getCallbackUrl());
				currentAddress = new UserAddresses(user,
						response.getInputAddress(),
						Constants.SUBSCRIPTION_CALLBACK_SECRET,
						response.getDestinationAddress(),
						response.getFeePercent(), response.getCallbackUrl());
				exchangeManager.create(currentAddress);
			} catch (Exception e) {
				log.error("Exception: " + e.getMessage());
				e.printStackTrace();
			}
		} else {
		}
		String code = new String(getParameter("code"));
		selectedSubscription = exchangeManager.getSubscriptionByCode(code)
				.getPrice().toString()
				+ " BTC";
	}

	/**
	 * Generate a Unique payment address for a User to send payment to
	 * https://blockchain.info/api/blockchain_wallet_api
	 * 
	 * @param params
	 *            Extra parameters to be passed to the callback URL
	 * @return Address
	 * @throws Exception
	 */
	private Address generatePaymentAddressWallet(Map<String, String> params)
			throws Exception {

		String url = Constants.SUBSCRIPTION_ROOT + "/merchant/"
				+ Constants.SUBSCRIPTION_WALLET_ID + "/new_address?";
		for (Map.Entry<String, String> param : params.entrySet()) {
			url += param.getKey() + "="
					+ URLEncoder.encode(param.getValue(), "UTF-8") + "&";
		}
		log.info("Calling Blockchain... " + url);
		String response = RESTUtils.getAPI(new API(null, null, url));
		if (response == null)
			throw new Exception("Server Returned NULL Response");
		JSONObject responseJSON = new JSONObject(response);
		if (responseJSON.has("error"))
			throw new Exception((String) responseJSON.getString("error"));
		return new Address(0L, responseJSON.getString("address"),
				responseJSON.getString("label"), 0L);
	}

	public void subscribe() {

		if (currentAddress == null) {
			/*
			 * Get Current Address from Blockchain
			 */
			Map<String, String> params = new HashMap<>();
			params.put("password", Constants.SUBSCRIPTION_WALLET_PASSWORD);
			params.put("second_password", "");
			params.put("label", user.getUsername());
			Address response;
			try {
				response = generatePaymentAddressWallet(params);
				log.info("Response: " + response.getAddress() + " "
						+ response.getLabel());
				currentAddress = new UserAddresses(user, response.getAddress(),
						Constants.SUBSCRIPTION_CALLBACK_SECRET,
						response.getAddress(), 0,
						Constants.SUBSCRIPTION_CALLBACK_URL);
				exchangeManager.create(currentAddress);
			} catch (Exception e) {
				log.error("Exception: " + e.getMessage());
				e.printStackTrace();
			}
		} else {
		}
		String code = new String(getParameter("code"));
		selectedSubscription = exchangeManager.getSubscriptionByCode(code)
				.getPrice().toString()
				+ " BTC";
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	private void getUserFromDB() {

		if (getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRemoteUser());
		} else {
			user = null;
		}
		if (user != null) {
			if (user.getUsername() != null) {
				user.setConfirmPassword(user.getPassword());
				if (isRememberMe()) {
					// if user logged in with remember me, display a warning
					// that they can't change passwords
					log.debug("checking for remember me login...");
					log.trace("User '" + user.getUsername()
							+ "' logged in with cookie");
					addFacesMessage("userProfile.cookieLogin");
				}
			}
		}
	}

	/**
	 * Convenience method for view templates to check if the user is logged in
	 * with RememberMe (cookies).
	 * 
	 * @return true/false - false if user interactively logged in.
	 */
	private boolean isRememberMe() {

		if (user != null && user.getId() == null)
			return false; // check for add()
		AuthenticationTrustResolver resolver = new AuthenticationTrustResolverImpl();
		SecurityContext ctx = SecurityContextHolder.getContext();
		if (ctx != null) {
			Authentication auth = ctx.getAuthentication();
			return resolver.isRememberMe(auth);
		}
		return false;
	}

	public List<SubscriptionUserView> getPaymentsHistory() {
		return paymentsHistory;
	}

	public void setPaymentsHistory(List<SubscriptionUserView> paymentsHistory) {
		this.paymentsHistory = paymentsHistory;
	}

	public List<UserAddresses> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<UserAddresses> addresses) {
		this.addresses = addresses;
	}

	public UserAddresses getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(UserAddresses currentAddress) {
		this.currentAddress = currentAddress;
	}

	public SubscriptionUserView getCurrentSubscription() {
		return currentSubscription;
	}

	public void setCurrentSubscription(SubscriptionUserView currentSubscription) {
		this.currentSubscription = currentSubscription;
	}

	public Subscription getSubscription1() {
		return subscription1;
	}

	public void setSubscription1(Subscription subscription1) {
		this.subscription1 = subscription1;
	}

	public Subscription getSubscription2() {
		return subscription2;
	}

	public void setSubscription2(Subscription subscription2) {
		this.subscription2 = subscription2;
	}

	public Subscription getSubscription3() {
		return subscription3;
	}

	public void setSubscription3(Subscription subscription3) {
		this.subscription3 = subscription3;
	}

	public Subscription getSubscription4() {
		return subscription4;
	}

	public void setSubscription4(Subscription subscription4) {
		this.subscription4 = subscription4;
	}

	public Subscription getSubscription5() {
		return subscription5;
	}

	public void setSubscription5(Subscription subscription5) {
		this.subscription5 = subscription5;
	}

	public String getSelectedSubscription() {
		return selectedSubscription;
	}

	public void setSelectedSubscription(String selectedSubscription) {
		this.selectedSubscription = selectedSubscription;
	}

	public Boolean getDisplaySubscriptions() {
		return displaySubscriptions;
	}

	public void setDisplaySubscriptions(Boolean displaySubscriptions) {
		this.displaySubscriptions = displaySubscriptions;
	}
}
