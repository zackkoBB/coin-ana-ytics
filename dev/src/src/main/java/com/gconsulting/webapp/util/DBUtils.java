package com.gconsulting.webapp.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.UserManager;


public class DBUtils {

	/**
	 * single instance of ArbitrageScanner
	 */
	private static final DBUtils INSTANCE = new DBUtils();
	private ExchangeManager exchangeManager;
	private UserManager userManager;

	private DBUtils() {

	}

	public static DBUtils getInstance() {
		return INSTANCE;
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	public ExchangeManager getExchangeManager() {
		return exchangeManager;
	}

	public UserManager getUserManager() {
		return userManager;
	}

}
