package com.gconsulting.webapp.model;

import com.gconsulting.model.Exchange;

public class Ticker {

	private Exchange exchange;
	private Order sellPrice;
	private Order buyPrice;
	private Orderbook orderbook;

	public Ticker() {
		super();
	}

	public Ticker(Exchange exchange, Order sellPrice, Order buyPrice,
			Orderbook orderbook) {
		super();
		this.exchange = exchange;
		this.sellPrice = sellPrice;
		this.buyPrice = buyPrice;
		this.orderbook = orderbook;
	}

	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	public Order getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Order sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Order getBuyPrice() {
		return buyPrice;
	}

	public void setBuyPrice(Order buyPrice) {
		this.buyPrice = buyPrice;
	}

	public Orderbook getOrderbook() {
		return orderbook;
	}

	public void setOrderbook(Orderbook orderbook) {
		this.orderbook = orderbook;
	}
}
