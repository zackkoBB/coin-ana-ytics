package com.gconsulting.webapp.exception;

public class ApplicationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3939035894580338076L;

	public ApplicationException() {
	}

	public ApplicationException(String message) {
		super(message);
	}
}
