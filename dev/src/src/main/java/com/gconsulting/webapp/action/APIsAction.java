package com.gconsulting.webapp.action;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.API;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiId;
import com.gconsulting.model.ids.FeeApiTypeId;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.util.ImportUtils;

@SuppressWarnings("unchecked")
@Scope("request")
@Component("apisAction")
public class APIsAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7276861502829761813L;
	private ExchangeManager exchangeManager;
	private API selectedAPI = new API();
	private UploadedFile importFile;
	private String exchangeCode;
	private String type;

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public APIsAction() {
		setSortColumn("exchange.code"); // sets the default sort column
	}

	public List<API> getAPIs() {
		return sort(exchangeManager.getAllApi());
	}

	public List<String> getExchanges() {

		List<String> exchangesString = new ArrayList<String>();
		List<Exchange> exchanges = exchangeManager.getAllExchange();
		for (Exchange exchange : exchanges) {
			exchangesString.add(exchange.getCode());
		}
		return exchangesString;
	}

	public List<String> getAPITypes() {

		List<String> typesString = new ArrayList<String>();
		List<FeeApiType> types = exchangeManager.getAllType();
		for (FeeApiType type : types) {
			typesString
					.add(type.getMarket().getCode() + " - " + type.getType());
		}
		return typesString;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public API getSelectedAPI() {
		return selectedAPI;
	}

	public void setSelectedAPI(API selectedAPI) {
		this.selectedAPI = selectedAPI;
	}

	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	public UploadedFile getImportFile() {
		return importFile;
	}

	public void setImportFile(UploadedFile importFile) {
		this.importFile = importFile;
	}

	public String importAPIs() {

		HttpServletRequest request = getRequest();
		// write the file to the filesystem
		// the directory to upload to
		String uploadDir = getFacesContext().getExternalContext().getRealPath(
				"/resources");
		// The following seems to happen when running jetty:run
		if (uploadDir == null) {
			uploadDir = new File("src/main/webapp/resources").getAbsolutePath();
		}
		uploadDir += "/" + request.getRemoteUser() + "/";
		try {
			String filename = importFile.getFileName();
			if (filename.endsWith(Constants.CSV_SUFFIX)) {
				// Create the CSVFormat object
				CSVFormat format = CSVFormat.RFC4180.withHeader()
						.withDelimiter(';');
				CSVParser parser = new CSVParser(new FileReader(
						ImportUtils.saveFile(importFile, uploadDir)), format);
				int i = 0, total = 0;
				String skippedSummary = new String();
				for (CSVRecord csvType : parser.getRecords()) {
					String marketCodeTemp = csvType
							.get(Constants.APIS_IMPORT_HEADER2);
					Market market = new Market();
					if (exchangeManager.getMarketByCode(marketCodeTemp) == null) {
						market = new Market(marketCodeTemp, marketCodeTemp
								+ " market");
						exchangeManager.create(market);
					} else {
						market = exchangeManager
								.getMarketByCode(marketCodeTemp);
					}
					String typeTemp = csvType
							.get(Constants.APIS_IMPORT_HEADER3);
					FeeApiType type = new FeeApiType();
					if (exchangeManager.getTypeById(new FeeApiTypeId(typeTemp,
							market)) == null) {
						type = new FeeApiType(typeTemp, market);
						exchangeManager.create(type);
					} else {
						type = exchangeManager.getTypeById(new FeeApiTypeId(
								typeTemp, market));
					}
					API api = new API();
					api.setExchange(exchangeManager.getExchangeByCode(csvType
							.get(Constants.APIS_IMPORT_HEADER1)));
					api.setFeeType(type);
					api.setAddress(csvType.get(Constants.APIS_IMPORT_HEADER4));
					if (exchangeManager.getApiById(new FeeApiId(api
							.getExchange(), api.getFeeType())) == null) {
						exchangeManager.create(api);
						i++;
					} else {
						if ((total - i) < Constants.SKIPPED_SUMMARY_LENGTH) {
							skippedSummary = skippedSummary.concat(api
									.getExchange().getCode()
									+ " "
									+ api.getFeeType().getMarket().getCode()
									+ " "
									+ api.getFeeType().getType()
									+ " "
									+ api.getAddress() + "\\n");
						}
					}
					total++;
				}
				// close the parser
				parser.close();
				addFacesMessage("apis.import.imported");
				addFacesMessage("apis.import.importedSummary", new Object[] { i,
						total - i, skippedSummary });
			} else {
				addFacesError("apis.import.error");
			}
		} catch (IOException e) {
			addFacesError("apis.import.error");
		}
		return "list";
	}

	public String delete() {

		exchangeManager.delete(selectedAPI);
		addFacesMessage("apis.deleted");
		return "list";
	}

	public String edit() {

		String typeType = null;
		String typeMarketCode = null;
		if (exchangeCode == null) {
			exchangeCode = new String(getParameter("exchangeCode"));
		}
		if (typeMarketCode == null) {
			typeMarketCode = new String(getParameter("marketCode"));
		}
		if (typeType == null) {
			typeType = new String(getParameter("type"));
		}
		type = typeMarketCode + " - " + typeType;
		Exchange selectedExchange = exchangeManager
				.getExchangeByCode(exchangeCode);
		Market selectedMarket = exchangeManager.getMarketByCode(typeMarketCode);
		FeeApiType apiType = new FeeApiType(typeType, selectedMarket);
		selectedAPI = exchangeManager.getApiById(new FeeApiId(selectedExchange,
				apiType));
		return "edit";
	}

	public String save() {

		String typeType = null;
		String typeMarketCode = null;
		String key = new String("");
		Exchange selectedExchange = exchangeManager
				.getExchangeByCode(exchangeCode);
		if (type != null) {
			if (type.length() > 0) {
				typeMarketCode = type.substring(0, type.indexOf(" - "));
				typeType = type.substring(type.indexOf(" - ") + 3);
			}
		}
		Market selectedMarket = new Market();
		if (exchangeManager.getMarketByCode(typeMarketCode) == null) {
			selectedMarket = new Market(typeMarketCode, typeMarketCode
					+ " market");
			exchangeManager.create(selectedMarket);
		} else {
			selectedMarket = exchangeManager.getMarketByCode(typeMarketCode);
		}
		FeeApiType apiType = new FeeApiType();
		if (exchangeManager.getTypeById(new FeeApiTypeId(typeType,
				selectedMarket)) == null) {
			apiType = new FeeApiType(typeType, selectedMarket);
			exchangeManager.create(apiType);
		} else {
			apiType = exchangeManager.getTypeById(new FeeApiTypeId(typeType,
					selectedMarket));
		}
		selectedAPI.setExchange(selectedExchange);
		selectedAPI.setFeeType(apiType);
		boolean isUpdate = (exchangeManager.getApiById(new FeeApiId(selectedAPI
				.getExchange(), selectedAPI.getFeeType())) == null) ? false
				: true;
		if (!isUpdate) {
			exchangeManager.create(selectedAPI);
			key = "apis.added";
		} else {
			exchangeManager.update(selectedAPI);
			key = "apis.updated";
		}
		addFacesMessage(key);
		return "list";
	}
}
