package com.gconsulting.webapp.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.primefaces.model.UploadedFile;

public class ImportUtils {

	public static File saveFile(UploadedFile importFile, String uploadDir)
			throws IOException {

		// Create the directory if it doesn't exist
		File dirPath = new File(uploadDir);
		if (!dirPath.exists()) {
			dirPath.mkdirs();
		}
		// retrieve the file data
		InputStream stream = importFile.getInputstream();
		String filename = importFile.getFileName();
		// APF-946: Canoo Web Tests R_1702 sets full path as name
		// instead of
		// only file name
		if (filename.contains("/")) {
			filename = filename.substring(filename.lastIndexOf("/") + 1);
		}
		// APF-758: Fix for Internet Explorer's shortcomings
		if (filename.contains("\\")) {
			int slash = filename.lastIndexOf("\\");
			if (slash != -1) {
				filename = filename.substring(slash + 1);
			}
			// Windows doesn't like /'s either
			int slash2 = filename.lastIndexOf("/");
			if (slash2 != -1) {
				filename = filename.substring(slash2 + 1);
			}
			// In case the name is C:foo.txt
			int slash3 = filename.lastIndexOf(":");
			if (slash3 != -1) {
				filename = filename.substring(slash3 + 1);
			}
		}
		// write the file to the file specified
		OutputStream bos = new FileOutputStream(uploadDir + filename);
		int bytesRead;
		byte[] buffer = new byte[8192];
		while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
			bos.write(buffer, 0, bytesRead);
		}
		bos.close();
		// close the stream
		stream.close();
		return new File(uploadDir + filename);
	}
}
