package com.gconsulting.webapp.action;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.Fee;
import com.gconsulting.model.Role;
import com.gconsulting.model.SubscriptionUser;
import com.gconsulting.model.User;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.UserManager;
import com.gconsulting.webapp.arbitrage.ArbitrageScanner;
import com.gconsulting.webapp.model.Arb;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;
import com.gconsulting.webapp.model.Ticker;

@ViewScoped
@Component("arbitrageEURAction")
public class ArbitrageEUR extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8079032464100412465L;
	private ExchangeManager exchangeManager;
	private Map<String, Orderbook> orderbooks;
	private List<Ticker> tickers;
	private Orderbook selectedOrderbook;
	private Exchange selectedExchange;
	private Map<String, Fee> tradingFees;
	private Map<String, Fee> transferFees;
	private int arbVolume;
	private List<Arb> arbs;
	private Arb selectedArb;
	private Orderbook selectedArbStartExchangeOrderbook;
	private Orderbook selectedArbEndExchangeOrderbook;
	private User user;

	public ArbitrageEUR() {
		super();
		setSortColumn("value"); // sets the default sort column
		arbVolume = Constants.ARB_BTC_SIZE1;
	}

	@PostConstruct
	public void init() {

		ArbitrageScanner.getInstance().setExchangeManager(exchangeManager);
		tradingFees = exchangeManager.getFIATTradingFee(exchangeManager
				.getAllExchange());
		transferFees = exchangeManager.getCRYPTOWithdrawFee(exchangeManager
				.getAllExchange());
		getArbsAll();
		selectArb();
	}

	/**
	 * Method to select an Arb Coming from xml: select in the View Coming from
	 * the Action: init
	 * 
	 */
	public void selectArb() {

		String selectedArbStartExchange = getParameter("selectedArbStartExchange");
		String selectedArbEndExchange = getParameter("selectedArbEndExchange");
		if ((selectedArbStartExchange != null)
				&& (selectedArbEndExchange != null)) {
			for (Arb arb : arbs) {
				if (arb.getStartExchange().getCode()
						.equalsIgnoreCase(selectedArbStartExchange)
						&& arb.getEndExchange().getCode()
								.equalsIgnoreCase(selectedArbEndExchange)) {
					selectedArb = arb;
					break;
				}
			}
		} else {
			/*
			 * I'm coming from inside the Action = Init
			 */
			if (arbs != null) {
				if (arbs.size() > 0)
					selectedArb = arbs.get(0);
				if (selectedArb != null) {
					if (selectedArb.getStartExchange() != null) {
						selectedArbStartExchange = selectedArb
								.getStartExchange().getCode();
					}
					if (selectedArb.getEndExchange() != null) {
						selectedArbEndExchange = selectedArb.getEndExchange()
								.getCode();
					}
				}
			}
		}
		if ((selectedArbStartExchange != null)
				&& (selectedArbEndExchange != null)) {
			selectedArbStartExchangeOrderbook = orderbooks
					.get(selectedArbStartExchange);
			selectedArbEndExchangeOrderbook = orderbooks
					.get(selectedArbEndExchange);
		}
	}

	public void updateArbs() {
		getArbsAll();
		selectArb();
	}

	/**
	 * Check for arbs: call all the REST before
	 * 
	 * @return List<Arb> retrieved
	 */
	private List<Arb> getArbsAll() {

		arbs = new ArrayList<>();
		tickers = new ArrayList<>();
		if (activeSubscription()) {
			orderbooks = ArbitrageScanner.getInstance().getOrderbooksEUR();
		} else {
			orderbooks = ArbitrageScanner.getInstance()
					.getOrderbooksEURDelayed();
		}
		if (orderbooks != null) {
			Set<String> keys = orderbooks.keySet();
			Orderbook referenceOrderbook;
			Orderbook compareOrderbook;
			for (String key : keys) {
				double referenceValue = 0;
				double referenceVolume = 0;
				/*
				 * Fill Tickers
				 */
				if (orderbooks.get(key) != null) {
					if ((orderbooks.get(key).getSellOrders().get(0) != null)
							&& (orderbooks.get(key).getBuyOrders().get(0) != null)) {
						tickers.add(new Ticker(exchangeManager
								.getExchangeByCode(key), orderbooks.get(key)
								.getSellOrders().get(0), orderbooks.get(key)
								.getBuyOrders().get(0), orderbooks.get(key)));
					}
				}
				referenceOrderbook = orderbooks.get(key);
				for (Order order : referenceOrderbook.getSellOrders()) {
					if ((referenceVolume + order.getQuantity()) > arbVolume) {
						/*
						 * Too much: go out
						 */
						// referenceVolume += (arbVolume - referenceVolume);
						referenceValue += order.getPrice()
								* (arbVolume - referenceVolume);
						referenceVolume += (arbVolume - referenceVolume);
						break;
					} else {
						/*
						 * Is fine: sum all the Volume
						 */
						referenceVolume += order.getQuantity();
						referenceValue += order.getPrice()
								* order.getQuantity();
					}
				}
				/*
				 * Start Fee: on the referenceVolume
				 */
				// referenceValue -= (referenceValue / 100.00)
				// * (new Double(tradingFees.get(key).getValue()));
				referenceVolume -= (referenceVolume / 100.00)
						* (new Double(tradingFees.get(key).getValue()));
				double receivedVolume = referenceVolume
						- (referenceVolume / 100.00)
						* (new Double(transferFees.get(key).getValue()));
				for (String subKey : keys) {
					/*
					 * Check for arbs: against all others Exchanges
					 */
					if (!key.equalsIgnoreCase(subKey)) {
						compareOrderbook = orderbooks.get(subKey);
						double compareValue = 0;
						double compareVolume = 0;
						for (Order order : compareOrderbook.getBuyOrders()) {
							if ((compareVolume + order.getQuantity()) > receivedVolume) {
								/*
								 * Too much: go out
								 */
								// compareVolume += (arbVolume - compareVolume);
								compareValue += order.getPrice()
										* (receivedVolume - compareVolume);
								compareVolume += (receivedVolume - compareVolume);
								break;
							} else {
								/*
								 * Is fine: sum all the Volume
								 */
								compareVolume += order.getQuantity();
								compareValue += order.getPrice()
										* order.getQuantity();
							}
						}
						/*
						 * End Fee: on the compareValue
						 */
						compareValue -= (compareValue / 100.00)
								* (new Double(tradingFees.get(subKey)
										.getValue()));
						if ((referenceValue > 0) && (compareValue > 0)) {
							if ((referenceValue < compareValue)
									&& (Math.abs(referenceVolume
											- compareVolume) <= Constants.VOLUME_PRECISION)) {
								/*
								 * ARB
								 */
								arbs.add(new Arb(
										new Double(
												(compareValue * 100.00 / referenceValue) - 100.00),
										null, exchangeManager
												.getExchangeByCode(key),
										tradingFees.get(key), exchangeManager
												.getExchangeByCode(subKey),
										tradingFees.get(subKey),
										referenceValue, referenceVolume,
										referenceValue / referenceVolume,
										compareVolume, compareValue,
										compareValue / compareVolume,
										transferFees.get(key), referenceVolume,
										receivedVolume));
							}
						}
					}
				}
			}
		}
		Collections.sort(arbs, new Comparator<Arb>() {
			@Override
			public int compare(Arb a1, Arb a2) {
				return (a1.getValue().doubleValue() > a2.getValue()
						.doubleValue()) ? 1 : -1;
			}
		});
		Collections.reverse(arbs);
		return arbs;
	}

	/**
	 * Check for arbs: call the REST interface programatically (need to check if
	 * the REST interface for that Exchange has been called already or not)
	 * 
	 * @return
	 */
	// private List<Arb> getArbsProgramatically() {
	//
	// return null;
	// }

	/**
	 * Check for arbs
	 * 
	 * @return
	 */
	public List<Arb> getArbs() {
		return arbs;
	}

	public Arb getSelectedArb() {
		return selectedArb;
	}

	public void setSelectedArb(Arb selectedArb) {
		this.selectedArb = selectedArb;
	}

	public Orderbook getSelectedArbStartExchangeOrderbook() {
		return selectedArbStartExchangeOrderbook;
	}

	public void setSelectedArbStartExchangeOrderbook(
			Orderbook selectedArbStartExchangeOrderbook) {
		this.selectedArbStartExchangeOrderbook = selectedArbStartExchangeOrderbook;
	}

	public Orderbook getSelectedArbEndExchangeOrderbook() {
		return selectedArbEndExchangeOrderbook;
	}

	public void setSelectedArbEndExchangeOrderbook(
			Orderbook selectedArbEndExchangeOrderbook) {
		this.selectedArbEndExchangeOrderbook = selectedArbEndExchangeOrderbook;
	}

	public void selectOrderbook() {

		String code = new String(getParameter("code"));
		selectedOrderbook = orderbooks.get(code);
		selectedExchange = exchangeManager.getExchangeByCode(code);
	}

	public int getArbVolume() {
		return arbVolume;
	}

	public void setArbVolume(int arbVolume) {
		this.arbVolume = arbVolume;
	}

	public List<Integer> getArbVolumes() {
		return Constants.getArbBTCSize();
	}

	public int getArbsNumber() {

		if (arbs != null) {
			return arbs.size();
		}
		return 0;
	}

	public boolean getArbsDisplay() {

		if (arbs != null) {
			if (arbs.size() > 0) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public List<Ticker> getTickers() {
		return tickers;
	}

	public void setTickers(List<Ticker> tickers) {
		this.tickers = tickers;
	}

	public Orderbook getSelectedOrderbook() {
		return selectedOrderbook;
	}

	public void setSelectedOrderbook(Orderbook selectedOrderbook) {
		this.selectedOrderbook = selectedOrderbook;
	}

	public Exchange getSelectedExchange() {
		return selectedExchange;
	}

	public void setSelectedExchange(Exchange selectedExchange) {
		this.selectedExchange = selectedExchange;
	}

	/**
	 * Get the current logged in user from DB
	 * 
	 * 
	 */
	private void getUserFromDB() {

		if (getRemoteUser() != null) {
			user = userManager.getUserByUsername(getRemoteUser());
		} else {
			user = null;
		}
	}

	/**
	 * Check active subscriptions for the current user
	 * 
	 * 
	 * @return true if a scubscription is active for the current user
	 */
	private boolean activeSubscription() {

		getUserFromDB();
		if (user != null) {
			for (Role role : user.getRoles()) {
				if (role.getName().equalsIgnoreCase(Constants.ADMIN_ROLE)) {
					return true;
				}
			}
			List<SubscriptionUser> subscriptionUser = exchangeManager
					.getSubscriptionUserByUser(user);
			/*
			 * Check for current active subscription
			 */
			Calendar now = Calendar.getInstance();
			long maxSubscriptionEndDate = now.getTimeInMillis() / 1000;
			for (SubscriptionUser subscription : subscriptionUser) {
				if (subscription.getEndDate() > (maxSubscriptionEndDate)) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean getDelayed() {
		return !activeSubscription();
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	@Override
	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}
}
