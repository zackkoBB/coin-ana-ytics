package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.webapp.model.ExchangeRate;

public class QUANDLInterface implements ExchangeRateRESTInterface {

	@Override
	public List<ExchangeRate> getExchangeRate(JSONObject source)
			throws JSONException {

		List<ExchangeRate> result = new ArrayList<>();
		JSONArray data = source.getJSONArray("data");
		for (int i = 0; i < 2; i++) {
			JSONArray jsonarray = data.getJSONArray(i);
			result.add(new ExchangeRate((String) jsonarray.get(0), new Double(
					(Double) jsonarray.get(1))));
		}
		return result;
	}

}
