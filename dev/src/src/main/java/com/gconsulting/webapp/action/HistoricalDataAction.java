package com.gconsulting.webapp.action;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.servlet.http.HttpServletRequest;

import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.gconsulting.Constants;
import com.gconsulting.model.Exchange;
import com.gconsulting.model.FeeApiType;
import com.gconsulting.model.HistoricalData;
import com.gconsulting.model.Market;
import com.gconsulting.model.ids.FeeApiTypeId;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.webapp.model.HistoricalDataList;
import com.gconsulting.webapp.util.ImportUtils;

@Scope("request")
@Component("historicalDataAction")
public class HistoricalDataAction extends BasePage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3470875354082763027L;
	private ExchangeManager exchangeManager;
	private UploadedFile importFile;
	private String exchangeCode;
	private String type;

	public HistoricalDataAction() {
	}

	/**
	 * Get HistoricalData to populate listHistoricalData page
	 * 
	 * 
	 * @return List<HistoricalDataList> info about the historicalData present in
	 *         the db
	 */
	public List<HistoricalDataList> getHistoricalData() {

		List<HistoricalDataList> result = new ArrayList<>();
		Exchange historicalDataExchange = exchangeManager
				.getExchangeByCode(Constants.HISTORICAL_DATA_EXCHANGE);
		List<FeeApiType> types = exchangeManager
				.getTypeByType(Constants.HISTORICAL_DATA_TYPE);
		for (FeeApiType type : types) {
			List<Long> data = exchangeManager
					.getHistoricalDataDataByExchangeAndType(
							historicalDataExchange, type);
			if (data != null) {
				if (data.size() > 0) {
					result.add(new HistoricalDataList(historicalDataExchange,
							type, data.get(0), data.get(1), data.get(2)));
				}
			}
		}
		return result;
	}

	/**
	 * Get all Exchanges to populate comboboxes
	 * 
	 * @return List<String> Exchanges codes list
	 */
	public List<String> getExchanges() {

		List<String> exchangesString = new ArrayList<String>();
		List<Exchange> exchanges = exchangeManager.getAllExchange();
		for (Exchange exchange : exchanges) {
			exchangesString.add(exchange.getCode());
		}
		return exchangesString;
	}

	/**
	 * Get historical Data types to populate comboboxes
	 * 
	 * @return List<String> historicalData types
	 */
	public List<String> getTypes() {

		List<String> typesString = new ArrayList<String>();
		List<FeeApiType> types = exchangeManager
				.getTypeByType(Constants.HISTORICAL_DATA_TYPE);
		for (FeeApiType type : types) {
			typesString
					.add(type.getMarket().getCode() + " - " + type.getType());
		}
		return typesString;
	}

	/**
	 * Import historical data
	 * 
	 * @return String redirect
	 */
	public String importHistoricalData() {

		HttpServletRequest request = getRequest();
		// write the file to the filesystem
		// the directory to upload to
		String uploadDir = getFacesContext().getExternalContext().getRealPath(
				"/resources");
		// The following seems to happen when running jetty:run
		if (uploadDir == null) {
			uploadDir = new File("src/main/webapp/resources").getAbsolutePath();
		}
		uploadDir += "/" + request.getRemoteUser() + "/";
		try {
			String filename = importFile.getFileName();
			if (filename.endsWith(Constants.CSV_SUFFIX)) {
				File inputFile = ImportUtils.saveFile(importFile, uploadDir);
				LineNumberReader lnr = new LineNumberReader(new FileReader(
						inputFile));
				lnr.skip(Long.MAX_VALUE);
				// System.out.println(lnr.getLineNumber() + 1); //Add 1 because
				// line index starts at 0
				long totalLines = lnr.getLineNumber() + 1;
				// Finally, the LineNumberReader object should be closed to
				// prevent resource leak
				lnr.close();
				Scanner scanner = new Scanner(inputFile);
				/*
				 * Get combo boxes
				 */
				Exchange historicalDataExchange = exchangeManager
						.getExchangeByCode(exchangeCode);
				String typeMarketCode = new String();
				String typeType = new String();
				if (type != null) {
					if (type.length() > 0) {
						typeMarketCode = type.substring(0, type.indexOf(" - "));
						typeType = type.substring(type.indexOf(" - ") + 3);
					}
				}
				Market historicalDataMarket = exchangeManager
						.getMarketByCode(typeMarketCode);
				FeeApiType historicalDataType = exchangeManager
						.getTypeById(new FeeApiTypeId(typeType,
								historicalDataMarket));
				HistoricalData historicalData = new HistoricalData();
				long inserted = 0, totalParsed = 0, maxTimestamp = 0;
				int progress = 5;
				double percentage = 0.0;
				String skippedSummary = new String();
				log.info("Total Lines: " + totalLines);
				List<Long> historicalDataData = exchangeManager
						.getHistoricalDataDataByExchangeAndType(
								historicalDataExchange, historicalDataType);
				if (historicalDataData != null) {
					if (historicalDataData.size() > 0) {
						maxTimestamp = historicalDataData.get(2);
					}
					log.info("historicalDataData: " + historicalDataData.get(0)
							+ " " + historicalDataData.get(1) + " "
							+ historicalDataData.get(2));
				}
				while (scanner.hasNextLine()) {
					String line = scanner.nextLine();
					String[] lineElements = line.split(",");
					/*
					 * Check for max(timestamp)
					 */
					if ((new Long(lineElements[0])) > maxTimestamp) {
						historicalData = new HistoricalData(null,
								historicalDataExchange, historicalDataType,
								(new Long(lineElements[0])), new Double(
										lineElements[1]), new Double(
										lineElements[2]));
						try {
							exchangeManager.create(historicalData);
							inserted++;
						} catch (Exception e) {
							if ((totalParsed - inserted) < Constants.SKIPPED_SUMMARY_LENGTH) {
								skippedSummary = skippedSummary
										.concat(historicalData.getExchange()
												.getCode()
												+ " "
												+ historicalData.getType()
														.getMarket().getCode()
												+ " "
												+ historicalData.getType()
														.getType() + "\\n");
							}
						}
					}
					totalParsed++;
					percentage = 100 * totalParsed / totalLines;
					if (percentage > progress) {
						log.info("Importing: " + percentage + "%");
						progress += 5;
					}
				}
				scanner.close();
				addFacesMessage("historicalData.import.imported");
				addFacesMessage("historicalData.import.importedSummary",
						new Object[] { inserted, totalParsed - inserted,
								skippedSummary });
			} else {
				addFacesError("historicalData.import.error");
			}
		} catch (IOException e) {
			addFacesError("historicalData.import.error");
		}
		return "list";
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getExchangeCode() {
		return exchangeCode;
	}

	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	public UploadedFile getImportFile() {
		return importFile;
	}

	public void setImportFile(UploadedFile importFile) {
		this.importFile = importFile;
	}
}
