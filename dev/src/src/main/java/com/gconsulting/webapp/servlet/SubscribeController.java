package com.gconsulting.webapp.servlet;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gconsulting.Constants;
import com.gconsulting.model.Subscription;
import com.gconsulting.model.SubscriptionUser;
import com.gconsulting.model.User;
import com.gconsulting.model.UserAddresses;
import com.gconsulting.service.ExchangeManager;
import com.gconsulting.service.UserManager;
import com.gconsulting.webapp.exception.ApplicationException;

@RestController
public class SubscribeController {

	protected final Log logger = LogFactory.getLog(getClass());
	private ExchangeManager exchangeManager;
	private UserManager userManager;
	// private User selectedUser;
	private SubscriptionUser addedSubscription;

	// private List<UserAddresses> userAddresses;
	// private List<Subscription> subscriptions;

	/**
	 * 
	 * http://79.31.252.114:8080/coin-analytics/rest/subscribe?
	 * input_transaction_hash
	 * =db0fb3ef45fd03625cb4e51842527823e35d60c1e2b28d1543c0d997d35eca37 &
	 * shared=false & address=1CgJ3M9kweRAhpjRWAZYvE3UL8cpTjAox1 &
	 * destination_address=1CgJ3M9kweRAhpjRWAZYvE3UL8cpTjAox1 &
	 * input_address=1GCZAh6MGsQG594HdLjCxhFGWhCHuu2LgZ & test=true &
	 * anonymous=false & confirmations=0 & user=user & value=1979990000 &
	 * transaction_hash
	 * =db0fb3ef45fd03625cb4e51842527823e35d60c1e2b28d1543c0d997d35eca37
	 *
	 * TODO: add OK response
	 * http://www.coin-analytics.com/rest/subscribe?input_address
	 * =1CgJ3M9kweRAhpjRWAZYvE3UL8cpTjAox1
	 * &secret=1A8JiWcwvpY7tAopUkSnGuEYHmzGYfZPiq
	 * &transaction_hash=1234&value=111111&confirmations=0
	 * 
	 */
	@RequestMapping(value = "/subscribe", method = RequestMethod.GET)
	public String getSubscription(
			// @RequestParam(value = "user") String user,
			@RequestParam(value = "transaction_hash") String transactionHash,
			// @RequestParam(value = "address") String address,
			@RequestParam(value = "input_address") String inputAddress,
			@RequestParam(value = "confirmations") String confirmations,
			@RequestParam(value = "value") String value,
			// @RequestParam(value = "test") String test,
			@RequestParam(value = "secret", defaultValue = Constants.SUBSCRIPTION_CALLBACK_SECRET) String secret)
			throws ApplicationException {

		String userId = new String();
		logger.info("New Subscription: " + inputAddress + " " + value + " "
				+ confirmations + " " + secret);
		if (new Integer(confirmations) > 0) {
			if (secret.equalsIgnoreCase(Constants.SUBSCRIPTION_CALLBACK_SECRET)) {
				List<UserAddresses> userAddresses = exchangeManager
						.getAllUserAddresses();
				for (UserAddresses userAddress : userAddresses) {
					if (userAddress.getAddress().equalsIgnoreCase(inputAddress)) {
						/*
						 * Found an address match!
						 */
						userId = userAddress.getUser().getId().toString();
					}
				}
				if (userId != null) {
					if (userId.length() > 0) {
						User selectedUser = userManager.getUser(userId);
						/*
						 * First of all check that the user paid to the right
						 * address
						 */
						// userAddresses = exchangeManager
						// .getUserAddressesByUser(selectedUser);
						/*
						 * boolean addressFound = false; for (UserAddresses
						 * userAddress : userAddresses) { if
						 * (userAddress.getAddress
						 * ().equalsIgnoreCase(inputAddress)) { addressFound =
						 * true; break; } } if (!addressFound) {
						 * logger.error("Address not found: " + inputAddress);
						 * throw new ApplicationException("Address not found: "
						 * + inputAddress); }
						 */
						/*
						 * Check which subscription was bought
						 */
						List<Subscription> subscriptions = exchangeManager
								.getAllSubscription();
						if (subscriptions != null) {
							if (subscriptions.size() > 0) {
								Collections.sort(subscriptions,
										new Comparator<Subscription>() {
											@Override
											public int compare(Subscription s1,
													Subscription s2) {
												return (s1.getPrice() > s2
														.getPrice()) ? 1 : -1;
											}
										});
								Collections.reverse(subscriptions);
								// value in BTC
								Double amount = new Double((new Long(value))) / 100000000.0;
								long now = Calendar.getInstance()
										.getTimeInMillis() / 1000;
								if (amount >= subscriptions.get(0).getPrice()) {
									/*
									 * Selected subscription = 24MONTHS
									 */
									addedSubscription = new SubscriptionUser(
											selectedUser, subscriptions.get(0),
											now
													+ subscriptions.get(0)
															.getDuration(),
											inputAddress, now);
								} else if (amount >= subscriptions.get(1)
										.getPrice()) {
									/*
									 * Selected subscription = 12MONTHS
									 */
									addedSubscription = new SubscriptionUser(
											selectedUser, subscriptions.get(1),
											now
													+ subscriptions.get(1)
															.getDuration(),
											inputAddress, now);
								} else if (amount >= subscriptions.get(2)
										.getPrice()) {
									/*
									 * Selected subscription = 6MONTHS
									 */
									addedSubscription = new SubscriptionUser(
											selectedUser, subscriptions.get(2),
											now
													+ subscriptions.get(2)
															.getDuration(),
											inputAddress, now);
								} else if (amount >= subscriptions.get(3)
										.getPrice()) {
									/*
									 * Selected subscription = 1MONTH
									 */
									addedSubscription = new SubscriptionUser(
											selectedUser, subscriptions.get(3),
											now
													+ subscriptions.get(3)
															.getDuration(),
											inputAddress, now);
								} else if (amount >= subscriptions.get(4)
										.getPrice()) {
									/*
									 * Selected subscription = 1DAY
									 */
									addedSubscription = new SubscriptionUser(
											selectedUser, subscriptions.get(4),
											now
													+ subscriptions.get(4)
															.getDuration(),
											inputAddress, now);
								} else if (amount < 0) {
									/*
									 * This is a withdraw
									 */
									logger.info("This is a withdraw.");
									return "*ok*";
								} else {
									logger.error("Value is not enough: "
											+ amount);
									throw new ApplicationException(
											"Value is not enough: " + amount);
								}
								/*
								 * Everything OK: save paid subscription
								 */
								exchangeManager.create(addedSubscription);
							}
						}

					} else {
						logger.error("User is null: " + userId);
						throw new ApplicationException("User is null: "
								+ userId);
					}
				} else {
					logger.error("User is null: " + userId);
					throw new ApplicationException("User is null: " + userId);
				}
			}
		} else {
			logger.error("Confirmations not enough: " + confirmations);
			throw new ApplicationException("Confirmations not enough: " + confirmations);
		}
		return "*ok*";
	}

	@Autowired
	public void setUserManager(@Qualifier("userManager") UserManager userManager) {
		this.userManager = userManager;
	}

	@Autowired
	public void setExchangeManager(
			@Qualifier("exchangeManager") ExchangeManager exchangeManager) {
		this.exchangeManager = exchangeManager;
	}
}