package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.Constants;
import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;

public class BTERInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source, String market)
			throws JSONException {
		return getOrderbookByMarket(source, market);
	}

	private Orderbook getOrderbookByMarket(JSONObject source, String market) {

		if (market.startsWith(Constants.MARKET_BTC_USD)) {
			return getOrderbookUSD(source);
		} else if (market.startsWith(Constants.MARKET_BTC_CNY)) {
			return getOrderbookCNY(source);
		}
		return null;
	}

	private Orderbook getOrderbookUSD(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
		JSONArray asks = source.getJSONArray("asks");
		JSONArray bids = source.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		double cumValue = 0.0;
		for (int i = asks.length() - 1; i >= 0; i--) {
			JSONArray jsonarray = asks.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else {
				amount = (Double) jsonarray.get(1);
			}
			cumValue += price * amount;
			sellOrders.add(new Order(price, amount, cumValue));
		}
		result.setSellOrders(sellOrders);
		cumValue = 0.0;
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else {
				amount = (Double) jsonarray.get(1);
			}
			cumValue += price * amount;
			buyOrders.add(new Order(price, amount, cumValue));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}

	private Orderbook getOrderbookCNY(JSONObject source) throws JSONException {

		Orderbook result = new Orderbook();
		JSONArray asks = source.getJSONArray("asks");
		JSONArray bids = source.getJSONArray("bids");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		double cumValue = 0.0;
		for (int i = asks.length() - 1; i >= 0; i--) {
			JSONArray jsonarray = asks.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else {
				amount = (Double) jsonarray.get(1);
			}
			cumValue += price * amount;
			sellOrders.add(new Order(price, amount, cumValue));
		}
		result.setSellOrders(sellOrders);
		cumValue = 0.0;
		for (int i = 0; i < bids.length(); i++) {
			JSONArray jsonarray = bids.getJSONArray(i);
			double price = 0.0;
			double amount = 0.0;
			if (jsonarray.get(0) instanceof Integer) {
				price = (new Integer((Integer) jsonarray.get(0))).doubleValue();
			} else {
				price = (Double) jsonarray.get(0);
			}
			if (jsonarray.get(1) instanceof Integer) {
				amount = (new Integer((Integer) jsonarray.get(1)))
						.doubleValue();
			} else if (jsonarray.get(1) instanceof Double) {
				amount = (Double) jsonarray.get(1);
			} else {
				amount = new Double((String) jsonarray.get(1));
			}
			cumValue += price * amount;
			buyOrders.add(new Order(price, amount, cumValue));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}

}
