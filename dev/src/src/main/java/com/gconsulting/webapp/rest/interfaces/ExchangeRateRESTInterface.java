package com.gconsulting.webapp.rest.interfaces;

import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.webapp.model.ExchangeRate;

public interface ExchangeRateRESTInterface {

	public List<ExchangeRate> getExchangeRate(JSONObject source) throws JSONException;
}
