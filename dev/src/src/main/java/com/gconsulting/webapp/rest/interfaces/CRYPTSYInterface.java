package com.gconsulting.webapp.rest.interfaces;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.gconsulting.model.Market;
import com.gconsulting.webapp.model.Order;
import com.gconsulting.webapp.model.Orderbook;

public class CRYPTSYInterface implements RESTInterface {

	@Override
	public List<Market> getPairs(JSONObject source) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Orderbook getOrderbook(JSONObject source, String market)
			throws JSONException {

		Orderbook result = new Orderbook();
		JSONObject returnObject = source.getJSONObject("return");
		JSONObject data = returnObject.getJSONObject("BTC");
		JSONArray asks = data.getJSONArray("sellorders");
		JSONArray bids = data.getJSONArray("buyorders");
		List<Order> sellOrders = new ArrayList<>();
		List<Order> buyOrders = new ArrayList<>();
		double cumValue = 0.0;
		double price = 0.0;
		double amount = 0.0;
		for (int i = 0; i < asks.length(); i++) {
			JSONObject jsonobject = asks.getJSONObject(i);
			price = jsonobject.getDouble("price");
			amount = jsonobject.getDouble("quantity");
			cumValue += price * amount;
			sellOrders.add(new Order(price, amount, cumValue));
		}
		result.setSellOrders(sellOrders);
		cumValue = 0.0;
		price = 0.0;
		amount = 0.0;
		for (int i = 0; i < bids.length(); i++) {
			JSONObject jsonobject = bids.getJSONObject(i);
			price = jsonobject.getDouble("price");
			amount = jsonobject.getDouble("quantity");
			cumValue += price * amount;
			buyOrders.add(new Order(price, amount, cumValue));
		}
		result.setBuyOrders(buyOrders);
		return result;
	}
}
