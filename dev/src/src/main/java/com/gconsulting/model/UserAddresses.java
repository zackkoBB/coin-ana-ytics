package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gconsulting.model.ids.UserAddressesId;

@Entity
@IdClass(UserAddressesId.class)
@Table(name = "user_addresses")
public class UserAddresses extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -774712434183122221L;
	private User user;
	private String address;
	private String secret;
	private String destinationAddress;
	private Integer feePercent;
	private String callbackURL;

	public UserAddresses() {
		super();
	}

	public UserAddresses(User user, String address, String secret,
			String destinationAddress, Integer feeParcent, String callbackURL) {
		super();
		this.user = user;
		this.address = address;
		this.secret = secret;
		this.destinationAddress = destinationAddress;
		this.feePercent = feeParcent;
		this.callbackURL = callbackURL;
	}

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Id
	@Column(name="address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="secret")
	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Column(name="destination_address")
	public String getDestinationAddress() {
		return destinationAddress;
	}

	public void setDestinationAddress(String destinationAddress) {
		this.destinationAddress = destinationAddress;
	}

	@Column(name="fee_percent")
	public Integer getFeePercent() {
		return feePercent;
	}

	public void setFeePercent(Integer feePercent) {
		this.feePercent = feePercent;
	}

	@Column(name="callback_url")
	public String getCallbackURL() {
		return callbackURL;
	}

	public void setCallbackURL(String callbackURL) {
		this.callbackURL = callbackURL;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
