package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscription")
public class Subscription extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8827707963863905205L;
	private String code;
	private String description;
	private Long duration;
	private Double price;

	public Subscription() {
		super();
	}

	public Subscription(String code, String description, Long duration,
			Double price) {
		super();
		this.code = code;
		this.description = description;
		this.duration = duration;
		this.price = price;
	}

	@Id
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
