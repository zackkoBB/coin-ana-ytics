package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
*
* @author <a href="mailto:gsit80@gmail.com">Giovanni Silvestri</a>
*/
@Entity
@Table(name = "historical_data")
public class HistoricalData extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2490724079000506487L;
	private Long id;
	private Exchange exchange;
	private FeeApiType type;
	private Long time;
	private Double price;
	private Double quantity;
	
	
	public HistoricalData() {
		super();
	}

	public HistoricalData(Long id, Exchange exchange, FeeApiType type, Long time,
			Double price, Double quantity) {
		super();
		this.id = id;
		this.exchange = exchange;
		this.type = type;
		this.time = time;
		this.price = price;
		this.quantity = quantity;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="exchange", referencedColumnName = "code")
	public Exchange getExchange() {
		return exchange;
	}

	public void setExchange(Exchange exchange) {
		this.exchange = exchange;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
			@JoinColumn(name = "type_type", referencedColumnName = "type"),
			@JoinColumn(name = "type_market", referencedColumnName = "market") })
	public FeeApiType getType() {
		return type;
	}

	public void setType(FeeApiType type) {
		this.type = type;
	}

	@Column(name = "timestamp")
	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	@Column(name = "price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name = "quantity")
	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
