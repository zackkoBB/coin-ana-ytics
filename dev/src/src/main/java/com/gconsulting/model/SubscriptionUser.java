package com.gconsulting.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gconsulting.model.ids.SubscriptionUserId;

@Entity
@IdClass(SubscriptionUserId.class)
@Table(name = "subscription_user")
public class SubscriptionUser extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4666860867485458020L;
	private User user;
	private Subscription subscription;
	private Long endDate;
	private String paidAddress;
	private Long startDate;

	public SubscriptionUser() {
		super();
	}

	public SubscriptionUser(User user, Subscription subscription, Long endDate,
			String paidAddress, Long startDate) {
		super();
		this.user = user;
		this.subscription = subscription;
		this.endDate = endDate;
		this.paidAddress = paidAddress;
		this.startDate = startDate;
	}

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user", referencedColumnName = "id")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Id
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "subscription", referencedColumnName = "code")
	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	@Id
	@Column(name = "end_date")
	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	@Column(name = "paid_address")
	public String getPaidAddress() {
		return paidAddress;
	}

	public void setPaidAddress(String paidAddress) {
		this.paidAddress = paidAddress;
	}

	@Column(name = "start_date")
	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
