package com.gconsulting.model.ids;

import java.io.Serializable;

import com.gconsulting.model.BaseObject;
import com.gconsulting.model.User;

public class UserAddressesId extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3641911839446118613L;
	private User user;
	private String address;

	public UserAddressesId() {
		super();
	}

	public UserAddressesId(User user, String address) {
		super();
		this.user = user;
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
