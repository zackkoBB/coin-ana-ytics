package com.gconsulting.model.ids;

import java.io.Serializable;

import com.gconsulting.model.BaseObject;
import com.gconsulting.model.Subscription;
import com.gconsulting.model.User;

public class SubscriptionUserId extends BaseObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4269467526032703775L;
	private User user;
	private Subscription subscription;
	private Long endDate;

	public SubscriptionUserId() {
		super();
	}

	public SubscriptionUserId(User user, Subscription subscription, Long endDate) {
		super();
		this.user = user;
		this.subscription = subscription;
		this.endDate = endDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return 0;
	}

}
